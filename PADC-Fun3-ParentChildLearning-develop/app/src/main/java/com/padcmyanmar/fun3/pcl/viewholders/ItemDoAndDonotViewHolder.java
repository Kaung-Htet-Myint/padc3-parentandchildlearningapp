package com.padcmyanmar.fun3.pcl.viewholders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.padcmyanmar.fun3.pcl.R;
import com.padcmyanmar.fun3.pcl.data.vo.ParentLearningVO;
import com.padcmyanmar.fun3.pcl.delegates.ParentDelegate;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by einandartun on 2/19/18.
 */

public class ItemDoAndDonotViewHolder extends BaseViewHolder {

    private ParentDelegate mParentDelegate;
    private ParentLearningVO mParentLearning;

    @BindView(R.id.iv_title_image)
    ImageView ivTitleImage;

    @BindView(R.id.tv_title)
    TextView tvTitle;

    public ItemDoAndDonotViewHolder(View itemView,ParentDelegate parentDelegate) {
        super(itemView);
        ButterKnife.bind(this,itemView);
        mParentDelegate = parentDelegate;
    }

    @Override
    public void setData(Object data) {
        mParentLearning = (ParentLearningVO) data;
        this.tvTitle.setText(mParentLearning.getTitle());

        Glide.with(ivTitleImage.getContext()).load(mParentLearning.getImageUrl())
                .into(ivTitleImage);

    }

    @Override
    public void onClick(View view) {

    }

    @OnClick(R.id.cv_do_and_donot)
    public void onTapDoAndDonotItem(View view){
        mParentDelegate.onTapDoAndDonotItem(mParentLearning);
//        Toast.makeText(view.getContext(),"detail",Toast.LENGTH_SHORT).show();
    }

}
