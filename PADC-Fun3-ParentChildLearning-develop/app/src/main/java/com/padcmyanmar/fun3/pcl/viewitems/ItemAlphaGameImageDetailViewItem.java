package com.padcmyanmar.fun3.pcl.viewitems;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import butterknife.ButterKnife;

/**
 * Created by htoo on 2/21/2018.
 */

public class ItemAlphaGameImageDetailViewItem extends FrameLayout {
    public ItemAlphaGameImageDetailViewItem(@NonNull Context context) {
        super(context);
    }

    public ItemAlphaGameImageDetailViewItem(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public ItemAlphaGameImageDetailViewItem(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this,this);
    }
}
