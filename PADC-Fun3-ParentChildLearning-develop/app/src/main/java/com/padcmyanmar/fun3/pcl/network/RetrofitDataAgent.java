package com.padcmyanmar.fun3.pcl.network;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.padcmyanmar.fun3.pcl.PCLApp;

import com.padcmyanmar.fun3.pcl.data.vo.BabyInfoVO;
import com.padcmyanmar.fun3.pcl.event.NetworkErrorEvent;
import com.padcmyanmar.fun3.pcl.event.StudentProfilesLoadedEvent;
import com.padcmyanmar.fun3.pcl.event.SuccessSaveBabyInfoEvent;
import com.padcmyanmar.fun3.pcl.network.responses.BabyInfoResponse;
import com.padcmyanmar.fun3.pcl.data.vo.CurrentWeightAndHeightVO;
import com.padcmyanmar.fun3.pcl.event.AddBabyDevelopmentEvent;
import com.padcmyanmar.fun3.pcl.event.BabyDevelopmentsEvent;
import com.padcmyanmar.fun3.pcl.event.LoadedChildLearningEvent;
import com.padcmyanmar.fun3.pcl.event.LoadedParentLearningEvent;
import com.padcmyanmar.fun3.pcl.event.SuccessLoginEvent;
import com.padcmyanmar.fun3.pcl.network.responses.AddBabyDevelopmentResponse;
import com.padcmyanmar.fun3.pcl.network.responses.GetBabyDevelopmentsResponse;
import com.padcmyanmar.fun3.pcl.network.responses.GetChildLearningResponse;
import com.padcmyanmar.fun3.pcl.network.responses.GetParentLearningResponse;
import com.padcmyanmar.fun3.pcl.network.responses.GetStudentProfilesResponse;
import com.padcmyanmar.fun3.pcl.network.responses.LoginResponse;
import com.padcmyanmar.fun3.pcl.network.responses.RegisterResponse;
import com.padcmyanmar.fun3.pcl.utils.AppConstants;

import org.greenrobot.eventbus.EventBus;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by daewichan on 3/4/18.
 */


public class RetrofitDataAgent implements ParentChildLearningDataAgent, LoginUserDataAgent {

    private static RetrofitDataAgent sObjectInstance;
    private ParentChildLearningApi learningApi;


    private RetrofitDataAgent() {
        OkHttpClient httpClient = new OkHttpClient.Builder()//1
                .connectTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(15, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()//1
                .baseUrl(AppConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .client(httpClient)
                .build();

        learningApi = retrofit.create(ParentChildLearningApi.class);
    }

    public static RetrofitDataAgent getsObjectInstance() {
        if (sObjectInstance == null) {
            sObjectInstance = new RetrofitDataAgent();
        }
        return sObjectInstance;
    }

    @Override
    public void loadLearning() {

        Call<GetChildLearningResponse> getLearningResponseCall = learningApi.loadChildLearning(1, "b002c7e1a528b7cb460933fc2875e916");

        getLearningResponseCall.enqueue(new Callback<GetChildLearningResponse>() {
            @Override
            public void onResponse(Call<GetChildLearningResponse> call, Response<GetChildLearningResponse> response) {
                GetChildLearningResponse getLearningResponse = response.body();
                if (getLearningResponse.getForChildren() != null) {
                    LoadedChildLearningEvent event = new LoadedChildLearningEvent(getLearningResponse.getForChildren());
                    EventBus.getDefault().post(event);
                }
            }

            @Override
            public void onFailure(Call<GetChildLearningResponse> call, Throwable t) {
                Log.e(PCLApp.LOG_TAG, "Retrofit Network" + t);
                LoadedChildLearningEvent event = new LoadedChildLearningEvent(null);
                EventBus.getDefault().post(event);
            }
        });

    }

    @Override
    public void loadParentLearning() {
        Call<GetParentLearningResponse> getParentLearningResponseCall = learningApi.loadParentLearning(1, "b002c7e1a528b7cb460933fc2875e916");

        getParentLearningResponseCall.enqueue(new Callback<GetParentLearningResponse>() {
            @Override
            public void onResponse(Call<GetParentLearningResponse> call, Response<GetParentLearningResponse> response) {
                GetParentLearningResponse getParentLearningResponse = response.body();
                if (getParentLearningResponse.getForParent() != null) {
                    LoadedParentLearningEvent event = new LoadedParentLearningEvent(getParentLearningResponse.getForParent());
                    EventBus.getDefault().post(event);
                }
            }

            @Override
            public void onFailure(Call<GetParentLearningResponse> call, Throwable t) {
                Log.e(PCLApp.LOG_TAG, "Retrofit Network" + t);
                NetworkErrorEvent event = new NetworkErrorEvent();
                EventBus.getDefault().post(event);
            }
        });
    }

    @Override
    public void registerUser(String name, String phoneNumber, String dateOfBirth, String country, String password) {
        Call<RegisterResponse> registerResponseCall = learningApi.registerUser(name, phoneNumber, dateOfBirth, country, password);
        registerResponseCall.enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                RegisterResponse registerResponse = response.body();
                if (registerResponse.getCode() == 200) {
                    EventBus.getDefault().post(registerResponse);
                }

            }

            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {
                Log.e(PCLApp.LOG_TAG, "Retrofit Network" + t);
            }
        });
    }

    @Override
    public void addBabyDevelopment(CurrentWeightAndHeightVO babyDevelopmentVO) {
        Call<AddBabyDevelopmentResponse> addBabyDevelopmentCall = learningApi.babyDevelopment(babyDevelopmentVO);
        addBabyDevelopmentCall.enqueue(new Callback<AddBabyDevelopmentResponse>() {
            @Override
            public void onResponse(Call<AddBabyDevelopmentResponse> call, Response<AddBabyDevelopmentResponse> response) {
                AddBabyDevelopmentResponse babyDevelopmentSuggestionResponse = response.body();
                if (babyDevelopmentSuggestionResponse.getBabyDevelopment() != null){
                    AddBabyDevelopmentEvent event = new AddBabyDevelopmentEvent(babyDevelopmentSuggestionResponse.getBabyDevelopment(),babyDevelopmentSuggestionResponse.getSuggestion());
                    EventBus.getDefault().post(event);
                }
            }

            @Override
            public void onFailure(Call<AddBabyDevelopmentResponse> call, Throwable t) {
                Log.e("Error call network", t.getMessage());
            }
        });
    }

    @Override
    public void loadBabyDevelopments() {
        Call<GetBabyDevelopmentsResponse> babyDevelopmentsResponseCall = learningApi.loadBabyDevelopments(1, "b002c7e1a528b7cb460933fc2875e916");
        babyDevelopmentsResponseCall.enqueue(new Callback<GetBabyDevelopmentsResponse>() {
            @Override
            public void onResponse(Call<GetBabyDevelopmentsResponse> call, Response<GetBabyDevelopmentsResponse> response) {
                GetBabyDevelopmentsResponse babyDevelopmentsResponse = response.body();
                if (babyDevelopmentsResponse.getBabyDevelopments() != null){
                    BabyDevelopmentsEvent event = new BabyDevelopmentsEvent(babyDevelopmentsResponse.getBabyDevelopments());
                    EventBus.getDefault().post(event);
                }
            }

            @Override
            public void onFailure(Call<GetBabyDevelopmentsResponse> call, Throwable t) {
                Log.e("Error call network", t.getMessage());
            }
        });
    }

    @Override
    public void loadStudentProfiles(String accessToken) {
        Call<GetStudentProfilesResponse> getStudentProfileCall = learningApi.loadStudentProfiles(accessToken);
        getStudentProfileCall.enqueue(new Callback<GetStudentProfilesResponse>() {
            @Override
            public void onResponse(Call<GetStudentProfilesResponse> call, Response<GetStudentProfilesResponse> response) {
                GetStudentProfilesResponse studentProfilesResponse = response.body();
                if (studentProfilesResponse != null){
                    StudentProfilesLoadedEvent event = new StudentProfilesLoadedEvent(studentProfilesResponse.getStudentProfiles());
                    EventBus.getDefault().post(event);
                }
            }

            @Override
            public void onFailure(Call<GetStudentProfilesResponse> call, Throwable t) {
                Log.e("Error call network", t.getMessage());
            }
        });
    }

    @Override
    public void LoginUser(String phoneNo, String password, final Context context) {

        final Call<LoginResponse> loginCall = learningApi.loginUser(phoneNo, password);
        loginCall.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                LoginResponse loginResponse = response.body();

                if (loginResponse != null) {
                    SuccessLoginEvent event = new SuccessLoginEvent(loginResponse.getLoginUser(), context);
                    EventBus.getDefault().post(event);
                }else {
                    SuccessLoginEvent event = new SuccessLoginEvent(loginResponse.getLoginUser(),context);
                    EventBus.getDefault().post(event);
                }

            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.d("Reftofit:", "Retrofit Network Call Failed.");
            }
        });

        /*LoginVO loginVO = new LoginVO(111,"KHM","khm@gmail.com","095113700","http://www.google.com","http://www.convertUrl.com");

        SuccessLoginEvent event = new SuccessLoginEvent(loginVO,context);
        EventBus.getDefault().post(event);*/

    }


    @Override
    public void  SaveBabyInfo(BabyInfoVO babyInfoVO){
        Call<BabyInfoResponse> babyInfoResponseCall = learningApi.SaveBabyInfo(babyInfoVO);
        babyInfoResponseCall.enqueue(new Callback<BabyInfoResponse>() {
            @Override
            public void onResponse(Call<BabyInfoResponse> call, Response<BabyInfoResponse> response) {
                BabyInfoResponse babyInfoResponse = response.body();
                if (babyInfoResponse.getCode() == 200){
                    SuccessSaveBabyInfoEvent event = new SuccessSaveBabyInfoEvent(babyInfoResponse.getBabyInfoVO());
                    EventBus.getDefault().post(event);
                }
            }

            @Override
            public void onFailure(Call<BabyInfoResponse> call, Throwable t) {
                Log.e(PCLApp.LOG_TAG, "BabyInfo Network" + t);
            }
        });
    }

}
