package com.padcmyanmar.fun3.pcl.data.model;

import android.widget.Toast;

import com.padcmyanmar.fun3.pcl.PCLApp;
import com.padcmyanmar.fun3.pcl.data.vo.BabyDevelopmentVO;
import com.padcmyanmar.fun3.pcl.event.BabyDevelopmentsEvent;
import com.padcmyanmar.fun3.pcl.network.ParentChildLearningDataAgent;
import com.padcmyanmar.fun3.pcl.network.RetrofitDataAgent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

/**
 * Created by htoo on 3/21/2018.
 */

public class BabyDevelopmentsModel {
    private static BabyDevelopmentsModel sObjInstance;
    private ParentChildLearningDataAgent mBabyDevelopmentDataAgent;

    private List<BabyDevelopmentVO> mBabyDevelopmentList;

    public BabyDevelopmentsModel() {
        mBabyDevelopmentDataAgent = RetrofitDataAgent.getsObjectInstance();

        EventBus.getDefault().register(this);
    }

    public static BabyDevelopmentsModel getsObjInstance() {
        if (sObjInstance == null){
            sObjInstance = new BabyDevelopmentsModel();
        }
        return sObjInstance;
    }

    public void loadBabyDevelopments(){
        mBabyDevelopmentDataAgent.loadBabyDevelopments();
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onBabyDevelopmentsLoaded(BabyDevelopmentsEvent event){
        mBabyDevelopmentList = event.getBabyDevelopmentList();
    }

    public List<BabyDevelopmentVO> getmBabyDevelopmentList(){
        return mBabyDevelopmentList;
    }
}
