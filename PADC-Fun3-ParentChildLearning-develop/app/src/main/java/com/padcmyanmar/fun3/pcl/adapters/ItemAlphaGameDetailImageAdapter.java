package com.padcmyanmar.fun3.pcl.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.padcmyanmar.fun3.pcl.R;
import com.padcmyanmar.fun3.pcl.viewitems.ItemAlphaGameImageDetailViewItem;

/**
 * Created by htoo on 2/21/2018.
 */

public class ItemAlphaGameDetailImageAdapter extends PagerAdapter {
    @Override
    public int getCount() {
        return 7;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        Context context = container.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        ItemAlphaGameImageDetailViewItem viewItem = (ItemAlphaGameImageDetailViewItem) layoutInflater.inflate(R.layout.item_alpha_game_detail_image,
                container,false);
        container.addView(viewItem);

        return viewItem;
    }


}
