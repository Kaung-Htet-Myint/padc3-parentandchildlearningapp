package com.padcmyanmar.fun3.pcl.activities;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.internal.InternalTokenProvider;
import com.padcmyanmar.fun3.pcl.R;
import com.padcmyanmar.fun3.pcl.data.model.BabyInfoModel;
import com.padcmyanmar.fun3.pcl.data.model.LoginUserModel;
import com.padcmyanmar.fun3.pcl.data.vo.BabyInfoVO;
import com.padcmyanmar.fun3.pcl.data.vo.LoginVO;
import com.padcmyanmar.fun3.pcl.event.SuccessSaveBabyInfoEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by einandartun on 2/21/18.
 */

public class BabyInfoActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.et_date_of_birth)
    EditText etDateOfBirth;

    @BindView(R.id.iv_baby_info)
    ImageView ivBabyInfo;

    @BindView(R.id.et_baby_name)
    EditText etBabyName;

    @BindView(R.id.et_guardian_name)
    EditText etGardianName;

    @BindView(R.id.rdo_group_gender)
    RadioGroup rdoGender;

    @BindView(R.id.et_weight)
    EditText etWeight;

    @BindView(R.id.et_height)
    EditText etHeight;

    Calendar calendar = Calendar.getInstance();

    private BabyInfoVO mBabyInfo;

   /* public static Intent newIntent(Context context)
    {
        Intent intent = new Intent(context,BabyInfoActivity.class);
        return intent;
    }*/


    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, BabyInfoActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_baby_info);
        ButterKnife.bind(this, this);

        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.baby_info_title);
        }
        mBabyInfo = new BabyInfoVO();

    }

    DatePickerDialog.OnDateSetListener dateOfBirth = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            String myFormat = "MM/dd/yy"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

            etDateOfBirth.setText(sdf.format(calendar.getTime()));

        }
    };

    @OnClick(R.id.et_date_of_birth)
    public void onClickDateofBirth(View view) {
        new DatePickerDialog(this, dateOfBirth, calendar
                .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    @OnClick(R.id.iv_baby_info)
    public void onClickProfileImage() {
        //Toast.makeText(getApplicationContext(), "On click img",Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.setType("image/*");
        startActivityForResult(intent, 234);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 234) {
            Uri originalUri = data.getData();
            mBabyInfo.setImage(Glide.with(getApplicationContext())
                    .load(originalUri)
                    .into(ivBabyInfo).toString());
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @OnClick(R.id.btn_save)
    public void onClickRegisterBabyInfo() {

        if (TextUtils.isEmpty(etBabyName.getText().toString())) {
            etBabyName.setError("Enter BabyName");
            return;
        }

        if (TextUtils.isEmpty(etGardianName.getText().toString())) {
            etGardianName.setError("Enter GardianName");
            return;
        }

        if (TextUtils.isEmpty(etDateOfBirth.getText().toString())) {
            etDateOfBirth.setError("Enter Date of Birth");
            return;
        }

        if (TextUtils.isEmpty(etWeight.getText().toString())) {
            etWeight.setError("Enter Weight");
            return;
        }

        if (TextUtils.isEmpty(etHeight.getText().toString())) {
            etHeight.setError("Enter Height");
            return;
        }

        LoginVO loginUser = LoginUserModel.getsObjInstance(getApplicationContext()).getLoginUser();

        mBabyInfo.setBabyId(1);
        mBabyInfo.setLoginUserId(loginUser.getUserId());
        mBabyInfo.setGuardianName(etGardianName.getText().toString());
        mBabyInfo.setDob(etDateOfBirth.getText().toString());

        int genderId = rdoGender.getCheckedRadioButtonId();
        RadioButton genderRadio = (RadioButton) findViewById(genderId);
        mBabyInfo.setGender(genderRadio.getText().toString());

        mBabyInfo.setWeight(Integer.parseInt(etWeight.getText().toString()));
        mBabyInfo.setHeight(Integer.parseInt(etHeight.getText().toString()));
        mBabyInfo.setImage("profile.png");

        BabyInfoModel.getsObjInstance(getApplicationContext()).SaveBabyInfo(mBabyInfo);

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBabyRegisterSuccess(SuccessSaveBabyInfoEvent event){
        Toast.makeText(getApplicationContext(),"Baby Info Save Successful.",Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
    }
}
