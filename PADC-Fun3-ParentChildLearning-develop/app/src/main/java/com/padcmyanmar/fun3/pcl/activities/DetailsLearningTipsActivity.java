package com.padcmyanmar.fun3.pcl.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.padcmyanmar.fun3.pcl.R;
import com.padcmyanmar.fun3.pcl.data.model.LoginUserModel;
import com.padcmyanmar.fun3.pcl.data.model.ParentLearningModel;
import com.padcmyanmar.fun3.pcl.data.vo.FavoriteUserVO;
import com.padcmyanmar.fun3.pcl.data.vo.LoginVO;
import com.padcmyanmar.fun3.pcl.data.vo.ParentLearningVO;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by daewichan on 2/21/18.
 */

public class DetailsLearningTipsActivity extends AppCompatActivity {
    @BindView(R.id.learning_toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_news_details)
    TextView tvNewsDetails;

    @BindView(R.id.iv_detail_learning)
    ImageView ivDetailImage;

    @BindView(R.id.tv_learning_title_details)
    TextView tvLearningTitle;

    @BindView(R.id.fab_favorite_news)
    FloatingActionButton favFavorite;

    String learningId, learningImage, learningTitle, learningBrief;

    private ParentLearningVO mParentLearning;

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, DetailsLearningTipsActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_learning_tips);
        ButterKnife.bind(this);


        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_24dp);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        /*learningId = getIntent().getStringExtra("id");//get id
        learningImage = getIntent().getStringExtra("learning_image");
        learningTitle = getIntent().getStringExtra("learning_title");
        learningBrief = getIntent().getStringExtra("learning_brief");

        showData();*/

        String learnId = getIntent().getStringExtra("id");

        ParentLearningVO parentLearning= ParentLearningModel.getsObjectInstance().getLearningById(Integer.valueOf(learnId));
        bindData(parentLearning);

    }

    private void bindData(ParentLearningVO parentLearningVO) {
        mParentLearning = parentLearningVO;
        tvNewsDetails.setText(parentLearningVO.getDescription());
        tvLearningTitle.setText(parentLearningVO.getTitle());
        Glide.with(ivDetailImage.getContext())
                .load(parentLearningVO.getImageUrl())
                .into(ivDetailImage);
    }

    private void showData() {


        tvNewsDetails.setText(learningBrief);
        tvLearningTitle.setText(learningTitle);
        Glide.with(ivDetailImage.getContext())
                .load(learningImage)
                .into(ivDetailImage);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) ;
        {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.fab_favorite_news)
    public void onTapFavorite(View view){
        LoginVO loginUser = LoginUserModel.getsObjInstance(view.getContext()).getLoginUser();
        FavoriteUserVO loginFavUser = new FavoriteUserVO();
        if (loginUser != null){
            loginFavUser.setUserId(loginUser.getUserId());
            loginFavUser.setUserName(loginUser.getName());

            List<FavoriteUserVO> favoriteUserList;
            favoriteUserList = ParentLearningModel.getsObjectInstance().getmParentLearningList().get(mParentLearning.getId()-1).getFavoriteUserList();
            for (FavoriteUserVO favUser : favoriteUserList){
                if (favUser.getUserId() == loginFavUser.getUserId()){
                    Toast.makeText(view.getContext(), R.string.already_exit, Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    ParentLearningModel.getsObjectInstance().getmParentLearningList().get(mParentLearning.getId()-1).getFavoriteUserList().add(loginFavUser);
                    favFavorite.setImageResource(R.drawable.ic_favorite_24dp);
                    return;
                }
            }
        }else {
            Snackbar.make(favFavorite,"Please Login First", Snackbar.LENGTH_SHORT).show();
        }
    }
}
