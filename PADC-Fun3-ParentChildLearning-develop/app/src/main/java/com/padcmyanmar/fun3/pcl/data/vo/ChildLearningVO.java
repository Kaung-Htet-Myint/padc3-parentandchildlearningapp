package com.padcmyanmar.fun3.pcl.data.vo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by daewichan on 3/4/18.
 */

public class ChildLearningVO {

    @Expose
    @SerializedName("id")
    private int id;

    @Expose
    @SerializedName("text")
    private String text;

    @Expose
    @SerializedName("image_url")
    private String imageUrl;

    @Expose
    @SerializedName("type")
    private int type;

    @SerializedName("color_code")
    private String colorCode;

    public int getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public int getType() {
        return type;
    }

    public String getColorCode() {
        return colorCode;
    }
}
