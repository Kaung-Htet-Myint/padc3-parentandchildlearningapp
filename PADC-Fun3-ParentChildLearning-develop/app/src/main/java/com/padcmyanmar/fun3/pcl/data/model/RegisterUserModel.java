package com.padcmyanmar.fun3.pcl.data.model;

import android.content.Context;
import android.content.SharedPreferences;

import com.padcmyanmar.fun3.pcl.data.vo.LoginVO;
import com.padcmyanmar.fun3.pcl.event.SuccessLoginEvent;
import com.padcmyanmar.fun3.pcl.event.UserLogoutEvent;
import com.padcmyanmar.fun3.pcl.network.ParentChildLearningDataAgent;
import com.padcmyanmar.fun3.pcl.network.RetrofitDataAgent;
import com.padcmyanmar.fun3.pcl.utils.AppConstants;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * Created by User on 3/8/2018.
 */

public class RegisterUserModel {
    private static RegisterUserModel sObjInstance;
    private ParentChildLearningDataAgent mDataAgent;
    private LoginVO mLogin;

    private RegisterUserModel(Context context) {
        mDataAgent = RetrofitDataAgent.getsObjectInstance();

        //EventBus.getDefault().register(this);
    }

    public static RegisterUserModel getsObjInstance(Context context) {
        if (sObjInstance == null) {
            sObjInstance = new RegisterUserModel(context);
        }
        return sObjInstance;
    }

    public void registerUser(String name, String phoneNo, String dateOfBirth, String country, String password) {

        mDataAgent.registerUser(name, phoneNo, dateOfBirth, country, password);
    }


//    public boolean isUserLogin(Context context) {
//        return mLogin != null;
//    }
//
//    public void logout(Context context) {
//        mLogin = null;
//        UserLogoutEvent event = new UserLogoutEvent();
//        EventBus.getDefault().post(event);
//
//    }
//
//    @Subscribe(threadMode = ThreadMode.BACKGROUND)
//    public void onLoginUserSucess(SuccessLoginEvent event) {
//        mLogin = event.getLogin();
//        //Save user data in SharedPreferences.
//        SharedPreferences sharedPreferences =
//                event.getContext().getSharedPreferences(AppConstants.LOGIN_USER_DATA_SP,
//                        Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = sharedPreferences.edit();
//        editor.putInt("login-user-id", event.getLogin().getUserId());
//        editor.putString("login-name", event.getLogin().getName());
//        editor.putString("login-email", event.getLogin().getEmail());
//        editor.putString("login-phone-no", event.getLogin().getPhoneNo());
//        editor.putString("login-profile-url", event.getLogin().getProfileUrl());
//        editor.putString("login-cover-url", event.getLogin().getCoverUrl());
//
//        //   editor.commit();
//        editor.apply();
//
//    }
//
//    public LoginVO getLoginUser() {
//        return mLogin;
//    }
}
