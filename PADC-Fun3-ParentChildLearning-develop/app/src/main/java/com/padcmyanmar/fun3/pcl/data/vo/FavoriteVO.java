package com.padcmyanmar.fun3.pcl.data.vo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by daewichan on 3/31/18.
 */

public class FavoriteVO {

    @SerializedName("user_id")
    private int userId;

    @SerializedName("user_name")
    private String userName;

    public int getUserId() {
        return userId;
    }

    public String getUserName() {
        return userName;
    }
}
