package com.padcmyanmar.fun3.pcl.event;

import android.content.Context;

import com.google.gson.annotations.SerializedName;
import com.padcmyanmar.fun3.pcl.data.vo.LoginVO;

/**
 * Created by User on 3/8/2018.
 */

public class SuccessLoginEvent {

    @SerializedName("login_user")
    private LoginVO login;

    private Context mContext;

    public Context getContext() {
        return mContext;
    }

    public LoginVO getLogin() {
        return login;
    }

    public SuccessLoginEvent(LoginVO login,Context context) {
        this.login = login;
        mContext = context;
    }
}
