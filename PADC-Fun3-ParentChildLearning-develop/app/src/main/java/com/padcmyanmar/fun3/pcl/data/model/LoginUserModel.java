package com.padcmyanmar.fun3.pcl.data.model;

import android.content.Context;
import android.content.SharedPreferences;

import com.padcmyanmar.fun3.pcl.data.vo.LoginVO;
import com.padcmyanmar.fun3.pcl.event.SuccessLoginEvent;
import com.padcmyanmar.fun3.pcl.event.UserLogoutEvent;
import com.padcmyanmar.fun3.pcl.network.LoginUserDataAgent;
import com.padcmyanmar.fun3.pcl.network.RetrofitDataAgent;
import com.padcmyanmar.fun3.pcl.utils.AppConstants;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * Created by User on 3/8/2018.
 */

public class LoginUserModel {
    private static LoginUserModel sObjInstance;
    private LoginUserDataAgent mDataAgent;
    private LoginVO mLogin;

    private LoginUserModel(Context context) {
        mDataAgent = RetrofitDataAgent.getsObjectInstance();

        EventBus.getDefault().register(this);

        SharedPreferences sharedPreferences = context.getSharedPreferences(AppConstants.LOGIN_USER_DATA_SP,
                Context.MODE_PRIVATE);
        int loginUserId = sharedPreferences.getInt(AppConstants.LOGIN_USER_ID, -1);
        if (loginUserId != -1) {
            //user has already login.
            String name = sharedPreferences.getString(AppConstants.LOGIN_USER_NAME, null);
            String address = sharedPreferences.getString(AppConstants.LOGIN_USER_ADDRESS, null);
            String email = sharedPreferences.getString(AppConstants.LOGIN_USER_EMAIL, null);
            String phoneNo = sharedPreferences.getString(AppConstants.LOGIN_USER_PHONE_NO, null);
            String profileUrl = sharedPreferences.getString(AppConstants.LOGIN_USER_PROFILE_URL, null);
            String coverUrl = sharedPreferences.getString(AppConstants.LOGIN_COVER_URL, null);

            mLogin = new LoginVO(loginUserId, name, address, email, phoneNo, profileUrl, coverUrl);
        }
    }

    public static LoginUserModel getsObjInstance(Context context) {
        if (sObjInstance == null) {
            sObjInstance = new LoginUserModel(context);
        }
        return sObjInstance;
    }

    public void LoginUser(String phoneNo, String password, Context context) {
        mDataAgent.LoginUser(phoneNo, password, context);
    }

    public boolean isUserLogin(Context context) {
        return mLogin != null;
    }

    public void logout(Context context) {
        mLogin = null;
        UserLogoutEvent event = new UserLogoutEvent();
        EventBus.getDefault().post(event);

    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onLoginUserSucess(SuccessLoginEvent event) {
    //    mLogin = new LoginVO(userId,name,address,phoneNo,email,profileUrl,coverUrl);
        mLogin = event.getLogin();
        //Save user data in SharedPreferences.
        SharedPreferences sharedPreferences =
                event.getContext().getSharedPreferences(AppConstants.LOGIN_USER_DATA_SP,
                        Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putInt(AppConstants.LOGIN_USER_ID, event.getLogin().getUserId());
        editor.putString(AppConstants.LOGIN_USER_NAME,event.getLogin().getName());
        editor.putString(AppConstants.LOGIN_USER_ADDRESS,event.getLogin().getAddress());
        editor.putString(AppConstants.LOGIN_USER_PHONE_NO,event.getLogin().getPhoneNo());
        editor.putString(AppConstants.LOGIN_USER_PROFILE_URL,event.getLogin().getProfileUrl());
        editor.putString(AppConstants.LOGIN_COVER_URL,event.getLogin().getCoverUrl());

        //   editor.commit();
        editor.apply();

    }

    public LoginVO getLoginUser() {
        return mLogin;
    }
}
