package com.padcmyanmar.fun3.pcl.network.responses;

import com.google.gson.annotations.SerializedName;
import com.padcmyanmar.fun3.pcl.data.vo.BabyInfoVO;

/**
 * Created by einandartun on 3/17/18.
 */

public class BabyInfoResponse {

    @SerializedName("code")
    private int code;

    @SerializedName("message")
    private String message;

    @SerializedName("apiVersion")
    private String apiVersion;

    @SerializedName("babyInfo")
    private BabyInfoVO babyInfoVO;

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String getApiVersion() {
        return apiVersion;
    }

    public BabyInfoVO getBabyInfoVO() {
        return babyInfoVO;
    }
}
