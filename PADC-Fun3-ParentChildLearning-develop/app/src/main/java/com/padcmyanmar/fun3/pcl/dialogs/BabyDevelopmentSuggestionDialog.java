package com.padcmyanmar.fun3.pcl.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.TextView;

import com.padcmyanmar.fun3.pcl.R;
import com.padcmyanmar.fun3.pcl.data.vo.BabyDevelopmentVO;

import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by htoo on 2/21/2018.
 */

public class BabyDevelopmentSuggestionDialog extends Dialog {

    @BindView(R.id.tv_summary_text)
    TextView tvSummaryText;

    public BabyDevelopmentSuggestionDialog(@NonNull Context context, BabyDevelopmentVO babyDevelopment) {
        super(context);
        setContentView(R.layout.dialog_baby_development_suggestion);

        ButterKnife.bind(this,this);

        tvSummaryText.setText(babyDevelopment.getOtherSuggestion().get(0).getSuggestion().concat(babyDevelopment.getOtherSuggestion().get(1).getSuggestion()));
    }
}
