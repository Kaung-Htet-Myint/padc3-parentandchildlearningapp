package com.padcmyanmar.fun3.pcl.data.model;

import com.padcmyanmar.fun3.pcl.data.vo.ParentLearningVO;
import com.padcmyanmar.fun3.pcl.data.vo.StudentProfileVO;
import com.padcmyanmar.fun3.pcl.event.LoadedParentLearningEvent;
import com.padcmyanmar.fun3.pcl.event.StudentProfilesLoadedEvent;
import com.padcmyanmar.fun3.pcl.network.ParentChildLearningDataAgent;
import com.padcmyanmar.fun3.pcl.network.RetrofitDataAgent;
import com.padcmyanmar.fun3.pcl.utils.AppConstants;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by daewichan on 3/9/18.
 */

public class ParentLearningModel {

    private static ParentLearningModel sObjectInstance;
    private ParentChildLearningDataAgent learningDataAgent;
    private Map<Integer, ParentLearningVO> pLearnings;
    private List<ParentLearningVO> mParentLearningList;

    private Map<String, StudentProfileVO> mStudentProfiles;

    private ParentLearningModel() {
        learningDataAgent = RetrofitDataAgent.getsObjectInstance();
        pLearnings = new HashMap<>();
        mParentLearningList = new ArrayList<>();

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        loadStudentProfiles();

//        pLearnings = new HashMap<>();
//        EventBus.getDefault().register(this);
    }

    public static ParentLearningModel getsObjectInstance() {
        if (sObjectInstance == null) {
            sObjectInstance = new ParentLearningModel();
        }
        return sObjectInstance;
    }

    public ParentLearningVO getLearningById(int learningId) {
        return pLearnings.get(learningId);

    }

    public void loadParentLearning() {
        learningDataAgent.loadParentLearning();
    }

    public void loadStudentProfiles() {
        learningDataAgent.loadStudentProfiles(AppConstants.TEMP_ACCESS_TOKEN);
    }

    public List<StudentProfileVO> getStudentProfiles() {
        if (mStudentProfiles == null)
            return new ArrayList<>();

        return new ArrayList<>(mStudentProfiles.values());
    }

    public StudentProfileVO getStudentByPhoneNo(String phoneNo) {
        if (mStudentProfiles == null)
            return null;

        return mStudentProfiles.get(phoneNo);
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onParentLearningLoaded(LoadedParentLearningEvent event) {
        mParentLearningList = event.getParentLearningList();

        for (ParentLearningVO parentLearning : event.getParentLearningList()) {
            pLearnings.put(parentLearning.getId(), parentLearning);

        }
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onStudentProfilesLoaded(StudentProfilesLoadedEvent event) {
        mStudentProfiles = new HashMap<>();
        for (StudentProfileVO studentProfile : event.getStudentProfiles()) {
            mStudentProfiles.put(studentProfile.getPhoneNo(), studentProfile);
        }
    }

    public List<ParentLearningVO> getmParentLearningList() {
        return mParentLearningList;
    }

}
