package com.padcmyanmar.fun3.pcl.fragments;

import android.content.Context;


import android.content.Intent;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.padcmyanmar.fun3.pcl.R;
import com.padcmyanmar.fun3.pcl.data.model.LoginUserModel;
import com.padcmyanmar.fun3.pcl.data.vo.LoginVO;
import com.padcmyanmar.fun3.pcl.delegates.LoginScreenDelegates;
import com.padcmyanmar.fun3.pcl.event.SuccessLoginEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 */

public class LoginFragment extends BaseFragment {

    @BindView(R.id.et_ph_no)
    EditText etPhNo;

    @BindView(R.id.et_password)
    EditText etPassword;

    LoginScreenDelegates mLoginScreenDelegates;

    private LoginVO mLogin;

    private String phoneNo = "", password = "";


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @OnClick(R.id.btn_login)
    public void onTapLoginButton(View view) {
        String phoneNo = etPhNo.getText().toString();
        String password = etPassword.getText().toString();

        if (TextUtils.isEmpty(phoneNo)) {
            etPhNo.setError("Enter phone number");
            return;
        }
        if (TextUtils.isEmpty(password)) {
            etPassword.setError("Enter Password");
            return;
        }

        mLoginScreenDelegates.onTapToLogin();

        LoginUserModel.getsObjInstance(getContext()).LoginUser(phoneNo, password, getContext());

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoginUserSucess(SuccessLoginEvent event) {
        if (getActivity() != null) {
            getActivity().onBackPressed();
        }
        if(event.getLogin().equals(null))
        {
            Toast.makeText(getActivity(),"Login Fail",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mLoginScreenDelegates = (LoginScreenDelegates) context;

    }

    @OnClick(R.id.btn_to_register)
    public void onTapToRegister(View view) {
        mLoginScreenDelegates.onTapTORegister();
    }

    @OnClick(R.id.btn_login_with_google)
    public void onTapLoginWithGoogle(View view) {
        mLoginScreenDelegates.onTapLoginWithGoogle();
    }

}
