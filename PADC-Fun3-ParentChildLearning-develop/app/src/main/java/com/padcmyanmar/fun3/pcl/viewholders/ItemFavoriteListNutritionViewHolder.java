package com.padcmyanmar.fun3.pcl.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.padcmyanmar.fun3.pcl.R;
import com.padcmyanmar.fun3.pcl.data.vo.ParentLearningVO;
import com.padcmyanmar.fun3.pcl.delegates.FavoriteDelegate;

import java.util.List;

import javax.microedition.khronos.opengles.GL;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by htoo on 3/14/2018.
 */

public class ItemFavoriteListNutritionViewHolder extends BaseViewHolder {

    private FavoriteDelegate mFavoriteDelegate;
    private ParentLearningVO favoriteNutritionVO;

    @BindView(R.id.tv_nutrition_title)
    TextView tvNutritionTitle;

    @BindView(R.id.tv_nutrition_brief)
    TextView tvNutritionBrief;

    @BindView(R.id.iv_nutrition_image)
    ImageView ivNutritionImage;

    public ItemFavoriteListNutritionViewHolder(View itemView, FavoriteDelegate favoriteDelegate) {
        super(itemView);
        ButterKnife.bind(this,itemView);

        mFavoriteDelegate = favoriteDelegate;
    }

    @Override
    public void setData(Object data) {
        favoriteNutritionVO = (ParentLearningVO) data;
        tvNutritionTitle.setText(favoriteNutritionVO.getTitle());
        tvNutritionBrief.setText(favoriteNutritionVO.getDescription());
        Glide.with(ivNutritionImage.getContext())
                .load(favoriteNutritionVO.getImageUrl())
                .into(ivNutritionImage);
    }

    @Override
    public void onClick(View view) {

    }

    @OnClick(R.id.cv_favorite_nutrition)
    public void onTapFavoriteNutrition(View view){
        mFavoriteDelegate.onTapFavoriteNutritionDetails(favoriteNutritionVO);
    }
}
