package com.padcmyanmar.fun3.pcl.data.model;

import com.padcmyanmar.fun3.pcl.data.vo.ChildLearningVO;
import com.padcmyanmar.fun3.pcl.event.LoadedChildLearningEvent;
import com.padcmyanmar.fun3.pcl.network.ParentChildLearningDataAgent;
import com.padcmyanmar.fun3.pcl.network.RetrofitDataAgent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by daewichan on 3/4/18.
 */

public class ChildLearningModel {
    private static ChildLearningModel sObjectInstance;
    private ParentChildLearningDataAgent mLearningDataAgent;
    private Map<String,ChildLearningVO> mChildLearning;
    private List<ChildLearningVO> mChildLearningList;

    private ChildLearningModel(){
        mLearningDataAgent = RetrofitDataAgent.getsObjectInstance();
        mChildLearning=new HashMap<>();
        mChildLearningList = new ArrayList<>();

        if(!EventBus.getDefault().isRegistered(this))
        {
            EventBus.getDefault().register(this);
        }
    }

    public static ChildLearningModel getsObjectInstance(){
        if (sObjectInstance==null){
            sObjectInstance=new ChildLearningModel();
        }
        return sObjectInstance;
    }

    public void loadLearning(){
        mLearningDataAgent.loadLearning();
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onChildNewsLoaded(LoadedChildLearningEvent event){
        mChildLearningList = event.getChildLearningList();
        for(ChildLearningVO childLearning : event.getChildLearningList()){
            mChildLearning.put(String.valueOf(childLearning.getId()),childLearning);
        }
    }

    public List<ChildLearningVO> getChildLearningList(){
        return mChildLearningList;
    }
}
