package com.padcmyanmar.fun3.pcl.data.model;


import com.padcmyanmar.fun3.pcl.data.vo.CurrentWeightAndHeightVO;
import com.padcmyanmar.fun3.pcl.data.vo.SuggestionVO;
import com.padcmyanmar.fun3.pcl.event.AddBabyDevelopmentEvent;
import com.padcmyanmar.fun3.pcl.network.ParentChildLearningDataAgent;
import com.padcmyanmar.fun3.pcl.network.RetrofitDataAgent;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * Created by htoo on 3/20/2018.
 */

public class AddBabyDevelopmentModel {
    private static AddBabyDevelopmentModel sObjInstance;
    private ParentChildLearningDataAgent mLearningDataAgent;
    private CurrentWeightAndHeightVO mBabyDevelopment;
    private SuggestionVO mSuggestion;


    private AddBabyDevelopmentModel(){
        mLearningDataAgent = RetrofitDataAgent.getsObjectInstance();
    }

    public static AddBabyDevelopmentModel getsObjInstance() {
        if (sObjInstance == null){
            sObjInstance = new AddBabyDevelopmentModel();
        }
        return sObjInstance;
    }

    public void addBabyDevelopment(CurrentWeightAndHeightVO babyDevelopment){
        mLearningDataAgent.addBabyDevelopment(babyDevelopment);
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void addBabyDevelopmentSuccess(AddBabyDevelopmentEvent babyDevelopmentEvent){
        mBabyDevelopment = babyDevelopmentEvent.getBabyDevelopment();
        mSuggestion = babyDevelopmentEvent.getSuggestion();
    }
}
