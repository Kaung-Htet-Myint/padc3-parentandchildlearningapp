package com.padcmyanmar.fun3.pcl.data.vo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by htoo on 3/18/2018.
 */

public class OtherSuggestionVO {

    @SerializedName("suggestion_id")
    private int suggestionID;

    @SerializedName("suggestion")
    private String suggestion;

    public int getSuggestionID() {
        return suggestionID;
    }

    public String getSuggestion() {
        return suggestion;
    }
}
