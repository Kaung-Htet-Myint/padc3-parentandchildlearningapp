package com.padcmyanmar.fun3.pcl.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.padcmyanmar.fun3.pcl.R;
import com.padcmyanmar.fun3.pcl.data.model.ChildLearningModel;
import com.padcmyanmar.fun3.pcl.data.vo.ChildLearningVO;
import com.padcmyanmar.fun3.pcl.event.LoadedChildLearningEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by daewichan on 2/19/18.
 */

public class ColorFragment extends Fragment implements TextToSpeech.OnInitListener {
    @BindView(R.id.iv_color)
    ImageView ivColor;

    @BindView(R.id.iv_red_color)
    ImageView ivRedColor;

    @BindView(R.id.view)
    View view;

    @BindView(R.id.iv_forward_arrow)
    ImageView ivForwardArrow;

    @BindView(R.id.iv_back_arrow)
    ImageView ivBackArrow;

    @BindView(R.id.iv_play)
    ImageView ivPlay;

    @BindView(R.id.tv_red)
    TextView tvRed;

    private TextToSpeech tts;

    private List<ChildLearningVO> mChildLearningList;

    private static ColorFragment sObjInstance;


    private int[] imgArr = {R.drawable.red, R.drawable.green, R.drawable.blue, R.drawable.yellow, R.drawable.pink, R.drawable.brown, R.drawable.gray, R.drawable.purple};
    private int[] imgArr1 = {R.drawable.red1, R.drawable.green1, R.drawable.blue1, R.drawable.yellow1, R.drawable.pink1, R.drawable.brown1, R.drawable.gray1, R.drawable.purple1};
    private String[] stringArr = {"RED", "GREEN", "BLUE", "YELLOW", "PINK", "BROWN", "GRAY", "PURPLE"};

    private int curIndex = 0;

    public static ColorFragment newInstance() {
        if (sObjInstance == null) {
            sObjInstance = new ColorFragment();
        }
        return sObjInstance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mChildLearningList = new ArrayList<>();

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_color, container, false);
        ButterKnife.bind(this, view);

//        ChildLearningModel.getsObjectInstance().loadLearning();


        tts = new TextToSpeech(getActivity(), this);

        hideBackArrow();

        mChildLearningList = ChildLearningModel.getsObjectInstance().getChildLearningList();

        changeColorView();

        return view;
    }


    private void showBackArrow() {
        ivBackArrow.setVisibility(View.VISIBLE);
    }

    private void hideBackArrow() {
        ivBackArrow.setVisibility(View.GONE);
    }

    private void showForwardArrow() {
        ivForwardArrow.setVisibility(View.VISIBLE);
    }

    private void hideForwardArrow() {
        ivForwardArrow.setVisibility(View.GONE);
    }

    @OnClick(R.id.view)
    public void onClickView(View view) {
        //speakOut();
    }

    private void changeColorView() {

        Log.d("data_size", mChildLearningList.size() + "");

        for (int i = 0; i < mChildLearningList.size(); i++) {
            if (mChildLearningList.get(i).getType() == 3) {

                curIndex = i;
                Log.d("image", mChildLearningList.get(i).getImageUrl() + "");
                Glide.with(this)
                        .load(mChildLearningList.get(i).getImageUrl())
                        .into(ivColor);


//                Glide.with(this)
//                        .load(mChildLearningList.get(i).getColorCode())
//                        .into(ivRedColor);

                ivRedColor.setBackgroundColor(Color.parseColor(mChildLearningList.get(i).getColorCode()));

                Log.d("color_code", mChildLearningList.get(i).getColorCode() + "");


                Log.d("title_text", mChildLearningList.get(i).getText());

                tvRed.setText(mChildLearningList.get(i).getText());

                break;
            }
        }

    }


    private void changeNextColor(ChildLearningVO childLearningVO) {

        if (childLearningVO.getType() == 3) {

            //delete after change color code of black(#000) and white(#fff)
            if (childLearningVO.getId()==22 || childLearningVO.getId()==23){
                return;
            }

            Glide.with(this)
                    .load(childLearningVO.getImageUrl())
                    .into(ivColor);

//            Glide.with(this)
//                    .load(childLearningVO.getColorCode())
//                    .into(ivRedColor);

            ivRedColor.setBackgroundColor(Color.parseColor(childLearningVO.getColorCode()));

            tvRed.setText(childLearningVO.getText());
        }

    }


    @OnClick(R.id.iv_forward_arrow)
    public void onClickArrow(View view) {

        if (curIndex < mChildLearningList.size()) {
            curIndex++;
            changeNextColor(mChildLearningList.get(curIndex));

            showBackArrow();

        } else {
            hideForwardArrow();
        }

    }

    @OnClick(R.id.iv_back_arrow)
    public void onClickBack(View view) {

        if (curIndex != 0) {
            curIndex--;

            changeNextColor(mChildLearningList.get(curIndex));

            showForwardArrow();
        } else {
            hideBackArrow();
        }
    }

    @OnClick(R.id.iv_play)
    public void onClickPaly(View view) {
        speakOut();
    }

    @Override
    public void onInit(int status) {

        if (status == TextToSpeech.SUCCESS) {

            int result = tts.setLanguage(Locale.US);

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported");
            } else {
                view.setEnabled(true);
                //speakOut();
            }

        } else {
            Log.e("TTS", "Initilization Failed!");
        }

    }

    private void speakOut() {
        String text = tvRed.getText().toString();
        tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onChildColorLoaded(LoadedChildLearningEvent event) {
        if (!event.getChildLearningList().isEmpty()) {
            mChildLearningList = event.getChildLearningList();
            //vpEmptyNews.setVisibility(View.GONE);
            //mNewsAdapter.setNews(event.getNewsList());

            changeColorView();

            Log.d("color_learning_response", mChildLearningList.size() + "");
        }

    }
}
