package com.padcmyanmar.fun3.pcl.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.padcmyanmar.fun3.pcl.R;
import com.padcmyanmar.fun3.pcl.activities.DetailsLearningTipsActivity;
import com.padcmyanmar.fun3.pcl.adapters.LearningTipsAdapter;
import com.padcmyanmar.fun3.pcl.data.model.ParentLearningModel;
import com.padcmyanmar.fun3.pcl.data.vo.ParentLearningVO;
import com.padcmyanmar.fun3.pcl.delegates.LearningTipsDelegate;
import com.padcmyanmar.fun3.pcl.event.LoadedParentLearningEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by daewichan on 2/19/18.
 */

public class LearningTipsFragment extends Fragment implements LearningTipsDelegate {
    @BindView(R.id.rv_learning_tips)
    RecyclerView rvLearningTips;

//    @BindView(R.id.tv_share)
//    TextView tvShare;


    private LearningTipsAdapter learningTipsAdapter;

    private List<ParentLearningVO> mParentLearningList;

    private ProgressDialog progressDialog;

    private static LearningTipsFragment sObjInstance;
    //private List<LearningTipsModel> learningTipsModels= new ArrayList<>();

    /**
     * for showing in main activity
     *
     * @return
     */
    public static LearningTipsFragment newInstance() {
        if (sObjInstance == null) {
            sObjInstance = new LearningTipsFragment();
        }
        return sObjInstance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mParentLearningList = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_learning_tips, container, false);
        ButterKnife.bind(this, view);

        learningTipsAdapter = new LearningTipsAdapter(this);


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        rvLearningTips.setAdapter(learningTipsAdapter);
        rvLearningTips.setLayoutManager(linearLayoutManager);

        //learningTipsAdapter.setLearningTips(LearningTipsModel.getsObjectInstance().loadLearningTips());

        //learningTipsAdapter.setLearningTips(learningTipsModels);


        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait while data loading");
        progressDialog.show();
        mParentLearningList = ParentLearningModel.getsObjectInstance().getmParentLearningList();
        if(!mParentLearningList.isEmpty()) {
            progressDialog.dismiss();
            List<ParentLearningVO> nutritionList = new ArrayList<>();

            for (int i = 0; i < mParentLearningList.size(); i++) {
                if (mParentLearningList.get(i).getType() == 2) {
                    nutritionList.add(mParentLearningList.get(i));
                }
                learningTipsAdapter.addAll(nutritionList);
            }
        } else {
            ParentLearningModel.getsObjectInstance().loadParentLearning();
        }

    }

    @Override
    public void onTapItem(ParentLearningVO parentLearningVO) {

        Log.d("tap_item", parentLearningVO.toString());

        Intent intent = new Intent(getContext(), DetailsLearningTipsActivity.class);
        intent.putExtra("id",String.valueOf(parentLearningVO.getId()));
        startActivity(intent);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onParentLoaded(LoadedParentLearningEvent event) {

        if (getActivity() != null) {
            progressDialog.dismiss();

            if (!event.getParentLearningList().isEmpty()) {
                mParentLearningList.clear();
                for (int i = 0; i < event.getParentLearningList().size(); i++) {
                    if (event.getParentLearningList().get(i).getType() == 2) {
                        mParentLearningList.add(event.getParentLearningList().get(i));
                    }
                }
                learningTipsAdapter.addAll(mParentLearningList);

            }

        }

    }
}
