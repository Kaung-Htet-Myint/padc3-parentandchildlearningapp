package com.padcmyanmar.fun3.pcl.event;

import com.padcmyanmar.fun3.pcl.data.vo.ChildLearningVO;
import com.padcmyanmar.fun3.pcl.data.vo.ParentLearningVO;

import java.util.List;

/**
 * Created by User on 3/8/2018.
 */

public class LoadedParentLearningEvent {
    private List<ParentLearningVO> mParentLearingList;

    public LoadedParentLearningEvent(List<ParentLearningVO> mParentLearingList ){
        this.mParentLearingList=mParentLearingList;
    }

    public List<ParentLearningVO> getParentLearningList(){
        return mParentLearingList;
    }
}
