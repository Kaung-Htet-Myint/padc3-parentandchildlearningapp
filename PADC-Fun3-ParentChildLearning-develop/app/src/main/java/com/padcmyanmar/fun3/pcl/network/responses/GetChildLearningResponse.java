package com.padcmyanmar.fun3.pcl.network.responses;

import com.google.gson.annotations.SerializedName;
import com.padcmyanmar.fun3.pcl.data.vo.ChildLearningVO;

import java.util.List;

/**
 * Created by daewichan on 3/3/18.
 */

public class GetChildLearningResponse {

    @SerializedName("code")
    private int code;

    @SerializedName("message")
    private String message;

    @SerializedName("apiVersion")
    private String apiVersion;

    @SerializedName("page")
    private String page;

    @SerializedName("forChildren")
    private List<ChildLearningVO> forChildren;


    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String getApiVersion() {
        return apiVersion;
    }

    public String getPage() {
        return page;
    }

    public List<ChildLearningVO> getForChildren() {
        return forChildren;
    }
}
