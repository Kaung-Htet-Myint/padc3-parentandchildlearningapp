package com.padcmyanmar.fun3.pcl.event;

import com.padcmyanmar.fun3.pcl.data.vo.LoginVO;

/**
 * Created by daewichan on 3/18/18.
 */

public class SuccessRegisterEvent {
    private LoginVO loginVO;

    public SuccessRegisterEvent(LoginVO loginVO){
        this.loginVO=loginVO;
    }

    public LoginVO getLoginVO() {
        return loginVO;
    }
}
