package com.padcmyanmar.fun3.pcl.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.padcmyanmar.fun3.pcl.R;
import com.padcmyanmar.fun3.pcl.data.vo.ParentLearningVO;
import com.padcmyanmar.fun3.pcl.delegates.FavoriteDelegate;
import com.padcmyanmar.fun3.pcl.viewholders.ItemFavoriteListLearningViewHolder;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

/**
 * Created by htoo on 3/14/2018.
 */

public class ItemFavoriteListLearningAdapter extends RecyclerView.Adapter<ItemFavoriteListLearningViewHolder> {

    private FavoriteDelegate mFavoriteDelegate;
    private List<ParentLearningVO> mFavoriteLearningList;

    public ItemFavoriteListLearningAdapter(FavoriteDelegate favoriteDelegate) {
        mFavoriteDelegate = favoriteDelegate;
        mFavoriteLearningList = new ArrayList<>();
    }

    @Override
    public ItemFavoriteListLearningViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.item_favorite_list_learning,parent,false);
        ItemFavoriteListLearningViewHolder viewHolder = new ItemFavoriteListLearningViewHolder(view,mFavoriteDelegate);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ItemFavoriteListLearningViewHolder holder, int position) {
        holder.setData(mFavoriteLearningList.get(position));
    }

    @Override
    public int getItemCount() {
        return mFavoriteLearningList.size();
    }

    public void setFavoriteLearningData(List<ParentLearningVO> favoriteLearningList){
        mFavoriteLearningList = favoriteLearningList;
        notifyDataSetChanged();
    }
}
