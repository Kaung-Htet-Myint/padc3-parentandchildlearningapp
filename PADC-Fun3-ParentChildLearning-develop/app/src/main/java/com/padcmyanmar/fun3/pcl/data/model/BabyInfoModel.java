package com.padcmyanmar.fun3.pcl.data.model;

import android.content.Context;

import com.padcmyanmar.fun3.pcl.data.vo.BabyInfoVO;
import com.padcmyanmar.fun3.pcl.network.ParentChildLearningDataAgent;
import com.padcmyanmar.fun3.pcl.network.RetrofitDataAgent;

import retrofit2.Retrofit;

/**
 * Created by einandartun on 3/18/18.
 */

public class BabyInfoModel {

    private static BabyInfoModel sObjInstance;
    private ParentChildLearningDataAgent mParentChildLearningDataAgent;

    private BabyInfoModel(Context context){
        mParentChildLearningDataAgent = RetrofitDataAgent.getsObjectInstance();
    }

    public static BabyInfoModel getsObjInstance(Context context) {
        if (sObjInstance == null) {
            sObjInstance = new BabyInfoModel(context);
        }
        return sObjInstance;
    }

//    public void SaveBabyInfo(int babyId, int loginUserId, String gardianName,String dob,String gender,int weight, int height, String Image){
//        mParentChildLearningDataAgent.SaveBabyInfo(babyId,loginUserId,gardianName,dob,gender,weight,height,Image);
//    }

    public void SaveBabyInfo(BabyInfoVO babyInfoVO){
        mParentChildLearningDataAgent.SaveBabyInfo(babyInfoVO);
    }

}
