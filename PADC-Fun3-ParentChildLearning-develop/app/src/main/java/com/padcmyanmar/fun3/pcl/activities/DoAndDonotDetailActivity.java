package com.padcmyanmar.fun3.pcl.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.preference.TwoStatePreference;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.padcmyanmar.fun3.pcl.R;
import com.padcmyanmar.fun3.pcl.data.model.LoginUserModel;
import com.padcmyanmar.fun3.pcl.data.model.ParentLearningModel;
import com.padcmyanmar.fun3.pcl.data.vo.FavoriteUserVO;
import com.padcmyanmar.fun3.pcl.data.vo.LoginVO;
import com.padcmyanmar.fun3.pcl.data.vo.ParentLearningVO;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by einandartun on 2/20/18.
 */

public class DoAndDonotDetailActivity extends BaseActivity {

    @BindView(R.id.iv_detail_image)
    ImageView ivDetailImage;

    @BindView(R.id.tv_details_title)
    TextView tvDetailTitle;

    @BindView(R.id.tv_details_main_content)
    TextView tvDetailMainContent;

    @BindView(R.id.fav_favourite_do_and_donnot)
    FloatingActionButton favFavorite;


    private ParentLearningVO mParentLearning;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_do_and_donot_details);
        ButterKnife.bind(this,this);
        String id = getIntent().getStringExtra("id_do_donot");
        mParentLearning = ParentLearningModel.getsObjectInstance().getLearningById(Integer.valueOf(id));
        bindParentData(mParentLearning);
    }

    private void bindParentData(ParentLearningVO parentLearning){
        mParentLearning = parentLearning;
        Glide.with(ivDetailImage.getContext()).load(parentLearning.getImageUrl()).into(ivDetailImage);
        tvDetailTitle.setText(parentLearning.getTitle());
        tvDetailMainContent.setText(parentLearning.getDescription());
    }


    @OnClick(R.id.fav_favourite_do_and_donnot)
    public void fabClick(View view){
        LoginVO loginUser = LoginUserModel.getsObjInstance(view.getContext()).getLoginUser();
        FavoriteUserVO favoriteUser = new FavoriteUserVO();

        if (loginUser != null){
            favoriteUser.setUserId(loginUser.getUserId());
            favoriteUser.setUserName(loginUser.getName());

            List<FavoriteUserVO> favoriteUserList;
            favoriteUserList = ParentLearningModel.getsObjectInstance().getmParentLearningList().get(mParentLearning.getId()-1).getFavoriteUserList();
            for (FavoriteUserVO favUser : favoriteUserList){
                if (favUser.getUserId() == favoriteUser.getUserId()){
                    Toast.makeText(view.getContext(), getResources().getString(R.string.already_exit), Toast.LENGTH_SHORT).show();
                    break;
                } else {
                    ParentLearningModel.getsObjectInstance().getmParentLearningList().get(mParentLearning.getId()-1).getFavoriteUserList().add(favoriteUser);
                    favFavorite.setImageResource(R.drawable.ic_favorite_24dp);
                    break;
                }
            }
        } else {
            Snackbar.make(favFavorite,"Please Login First", Snackbar.LENGTH_SHORT).show();
        }


    }
}
