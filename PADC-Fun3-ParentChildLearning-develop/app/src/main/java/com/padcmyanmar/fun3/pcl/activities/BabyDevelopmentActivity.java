package com.padcmyanmar.fun3.pcl.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.padcmyanmar.fun3.pcl.R;
import com.padcmyanmar.fun3.pcl.data.model.AddBabyDevelopmentModel;
import com.padcmyanmar.fun3.pcl.data.model.BabyDevelopmentsModel;
import com.padcmyanmar.fun3.pcl.data.vo.BabyDevelopmentVO;
import com.padcmyanmar.fun3.pcl.data.vo.CurrentWeightAndHeightVO;
import com.padcmyanmar.fun3.pcl.dialogs.BabyDevelopmentSuggestionDialog;
import com.padcmyanmar.fun3.pcl.event.AddBabyDevelopmentEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by htoo on 2/21/2018.
 */

public class BabyDevelopmentActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.et_weight)
    EditText etWeight;
    @BindView(R.id.et_height)
    EditText etHeight;


    public static Intent newIntent(Context context){
        Intent intent = new Intent(context, BabyDevelopmentActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_baby_development);

        ButterKnife.bind(this,this);

        addToolbar();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    private void addToolbar() {
        setSupportActionBar(toolbar);

        if(getSupportActionBar() != null){
            getSupportActionBar().setTitle(R.string.baby_development_title);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_24dp);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btn_save_baby_development)
    public void onTapBabyDevelopmentSave(View view){
//        Intent intent = BabyDevelopmentListActivity.newIntent(getApplicationContext());
//        startActivity(intent);
        CurrentWeightAndHeightVO babyDevelopment = new CurrentWeightAndHeightVO();
        babyDevelopment.setCurrentWeight(Double.valueOf(etWeight.getText().toString()));
        babyDevelopment.setCurrentHeight(Double.valueOf(etHeight.getText().toString()));

        AddBabyDevelopmentModel.getsObjInstance().addBabyDevelopment(babyDevelopment);

        /*BabyDevelopmentSuggestionDialog dialog = new BabyDevelopmentSuggestionDialog(this, );
        dialog.show();*/
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRegisterSuccess(AddBabyDevelopmentEvent event){
        BabyDevelopmentVO babyDevelopment = new BabyDevelopmentVO();
        babyDevelopment.setActualWeight(event.getBabyDevelopment().getCurrentWeight());
        babyDevelopment.setActualHeight(event.getBabyDevelopment().getCurrentHeight());
        babyDevelopment.setSuggestionWeight(event.getSuggestion().getSuggestionWeight());
        babyDevelopment.setSuggestionHeight(event.getSuggestion().getIntsuggestionHeight());
        babyDevelopment.setOtherSuggestion(event.getSuggestion().getOtherSuggestionList());

        BabyDevelopmentsModel.getsObjInstance().getmBabyDevelopmentList().add(babyDevelopment);
        Toast.makeText(getApplicationContext(),"Save Successful.", Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(getApplicationContext(), BabyDevelopmentListActivity.class);
        startActivity(intent);
    }
}
