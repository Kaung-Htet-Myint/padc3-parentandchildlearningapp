package com.padcmyanmar.fun3.pcl.event;

import com.padcmyanmar.fun3.pcl.data.vo.BabyDevelopmentVO;

import java.util.List;

/**
 * Created by htoo on 3/21/2018.
 */

public class BabyDevelopmentsEvent {
    List<BabyDevelopmentVO> babyDevelopmentList;

    public BabyDevelopmentsEvent(List<BabyDevelopmentVO> babyDevelopmentList) {
        this.babyDevelopmentList = babyDevelopmentList;
    }

    public List<BabyDevelopmentVO> getBabyDevelopmentList() {
        return babyDevelopmentList;
    }
}
