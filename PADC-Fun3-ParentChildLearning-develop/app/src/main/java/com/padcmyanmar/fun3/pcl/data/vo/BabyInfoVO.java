package com.padcmyanmar.fun3.pcl.data.vo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by einandartun on 3/17/18.
 */

public class BabyInfoVO {

    private int babyId;

    @SerializedName("login_user_id")
    private int loginUserId;

    private String babyName;

    public String getBabyName() {
        return babyName;
    }

    @SerializedName("guardian_name")
    private String guardianName;

    private String dob;

    private String gender;

    private int weight;

    private int height;

    private String image;

    public int getBabyId() {
        return babyId;
    }

    public int getLoginUserId() {
        return loginUserId;
    }

    public String getGuardianName() {
        return guardianName;
    }

    public String getDob() {
        return dob;
    }

    public String getGender() {
        return gender;
    }

    public int getWeight() {
        return weight;
    }

    public int getHeight() {
        return height;
    }

    public String getImage() {
        return image;
    }

    public void setBabyId(int babyId) {
        this.babyId = babyId;
    }

    public void setLoginUserId(int loginUserId) {
        this.loginUserId = loginUserId;
    }

    public void setGuardianName(String guardianName) {
        this.guardianName = guardianName;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setBabyName(String babyName) {
        this.babyName = babyName;
    }
}
