package com.padcmyanmar.fun3.pcl.viewholders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.padcmyanmar.fun3.pcl.R;
import com.padcmyanmar.fun3.pcl.adapters.ItemFavoriteListLearningAdapter;
import com.padcmyanmar.fun3.pcl.data.model.LoginUserModel;
import com.padcmyanmar.fun3.pcl.data.model.ParentLearningModel;
import com.padcmyanmar.fun3.pcl.data.vo.FavoriteUserVO;
import com.padcmyanmar.fun3.pcl.data.vo.LoginVO;
import com.padcmyanmar.fun3.pcl.data.vo.ParentLearningVO;
import com.padcmyanmar.fun3.pcl.delegates.FavoriteDelegate;

import org.w3c.dom.Text;

import javax.microedition.khronos.opengles.GL;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by htoo on 3/14/2018.
 */

public class ItemFavoriteListLearningViewHolder extends BaseViewHolder {

    private FavoriteDelegate mFavoriteDelegate;
    private ParentLearningVO mFavoriteLearningVO;

    @BindView(R.id.iv_learning_educate)
    ImageView ivLearningEducate;

    @BindView(R.id.tv_learning_sub_title)
    TextView tvLearningSubTitle;

    @BindView(R.id.tv_learning_brief)
    TextView tvLearningBrief;

    @BindView(R.id.btn_favourite)
    TextView btnFavorite;

    public ItemFavoriteListLearningViewHolder(View itemView, FavoriteDelegate favoriteDelegate) {
        super(itemView);
        ButterKnife.bind(this,itemView);

        mFavoriteDelegate = favoriteDelegate;
    }

    @Override
    public void setData(Object data) {
        mFavoriteLearningVO = (ParentLearningVO) data;

        tvLearningSubTitle.setText(mFavoriteLearningVO.getTitle());
        Glide.with(ivLearningEducate.getContext())
                .load(mFavoriteLearningVO.getImageUrl())
                .into(ivLearningEducate);
        tvLearningBrief.setText(mFavoriteLearningVO.getDescription());
    }

    @Override
    public void onClick(View view) {

    }

    @OnClick(R.id.cv_favorite_learning)
    public void onTapFavoriteLearning(View view){
        mFavoriteDelegate.onTapFavoriteLearningDetails(mFavoriteLearningVO);
    }

    @OnClick(R.id.btn_favourite)
    public void onTapFavoriteList(View view){
        Toast.makeText(view.getContext(), "Click", Toast.LENGTH_SHORT).show();
        /*LoginVO loginUser = LoginUserModel.getsObjInstance(view.getContext()).getLoginUser();
        FavoriteUserVO favoriteUser = new FavoriteUserVO();
        favoriteUser.setUserId(loginUser.getUserId());
        favoriteUser.setUserName(loginUser.getName());

        ParentLearningModel.getsObjectInstance().getmParentLearningList().get(mFavoriteLearningVO.getId()-1).getFavoriteUserList().add(favoriteUser);

        int favorite = R.drawable.ic_favorite_border_24dp;
        if (!(btnFavorite.getCompoundDrawables().equals(favorite))){
            btnFavorite.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_favorite_24dp,0,0,0);
        } else {
            btnFavorite.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_favorite_border_24dp,0,0,0);
        }*/
    }
}
