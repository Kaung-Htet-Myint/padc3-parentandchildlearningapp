package com.padcmyanmar.fun3.pcl.event;

import com.padcmyanmar.fun3.pcl.data.vo.CurrentWeightAndHeightVO;
import com.padcmyanmar.fun3.pcl.data.vo.SuggestionVO;

/**
 * Created by htoo on 3/20/2018.
 */

public class AddBabyDevelopmentEvent {
    private CurrentWeightAndHeightVO babyDevelopment;
    private SuggestionVO suggestion;

    public AddBabyDevelopmentEvent(CurrentWeightAndHeightVO babyDevelopment, SuggestionVO suggestion) {
        this.babyDevelopment = babyDevelopment;
        this.suggestion = suggestion;
    }

    public CurrentWeightAndHeightVO getBabyDevelopment() {
        return babyDevelopment;
    }

    public SuggestionVO getSuggestion() {
        return suggestion;
    }
}
