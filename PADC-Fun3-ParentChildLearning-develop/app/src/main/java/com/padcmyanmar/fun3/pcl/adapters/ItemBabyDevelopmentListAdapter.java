package com.padcmyanmar.fun3.pcl.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.padcmyanmar.fun3.pcl.R;
import com.padcmyanmar.fun3.pcl.data.vo.BabyDevelopmentVO;
import com.padcmyanmar.fun3.pcl.delegates.BabyDevelopmentDelegate;
import com.padcmyanmar.fun3.pcl.viewholders.ItemBabyDevelopmentListViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by htoo on 2/21/2018.
 */

public class ItemBabyDevelopmentListAdapter extends RecyclerView.Adapter<ItemBabyDevelopmentListViewHolder> {

    private BabyDevelopmentDelegate mBabyDevelopmentDelegate;
    private List<BabyDevelopmentVO> mBabyDevelopmentList;

    public ItemBabyDevelopmentListAdapter(BabyDevelopmentDelegate babyDevelopmentDelegate){
        mBabyDevelopmentDelegate = babyDevelopmentDelegate;
        mBabyDevelopmentList = new ArrayList<>();
    }


    @Override
    public ItemBabyDevelopmentListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.item_baby_development_list, parent, false);
        ItemBabyDevelopmentListViewHolder viewHolder = new ItemBabyDevelopmentListViewHolder(view,mBabyDevelopmentDelegate);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ItemBabyDevelopmentListViewHolder holder, int position) {
        holder.setData(mBabyDevelopmentList.get(position));
    }

    @Override
    public int getItemCount() {
        return mBabyDevelopmentList.size();
    }

    public void setBabyDevelopmentData(List<BabyDevelopmentVO> babyDevelopmentList){
        mBabyDevelopmentList = babyDevelopmentList;
        notifyDataSetChanged();
    }
}
