package com.padcmyanmar.fun3.pcl.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.padcmyanmar.fun3.pcl.R;
import com.padcmyanmar.fun3.pcl.adapters.DoAndDonotAdapter;
import com.padcmyanmar.fun3.pcl.adapters.ParentAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by einandartun on 2/19/18.
 */

public class ParentFragment extends BaseFragment {

    @BindView(R.id.rv_do_and_donot)
    RecyclerView rvDoAndDonot;

    private DoAndDonotAdapter mDoAndDonotAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_do_and_donot,container,false);
        ButterKnife.bind(this,view);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        rvDoAndDonot.setLayoutManager(linearLayoutManager);
        rvDoAndDonot.setAdapter(mDoAndDonotAdapter);


        return view;
    }
}
