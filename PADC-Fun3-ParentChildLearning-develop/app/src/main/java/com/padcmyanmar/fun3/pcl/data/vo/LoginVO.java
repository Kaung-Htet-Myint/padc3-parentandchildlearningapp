package com.padcmyanmar.fun3.pcl.data.vo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by User on 3/8/2018.
 */

public class LoginVO {
    @SerializedName("user_id")
    private int userId;

    @SerializedName("name")
    private String name;

    @SerializedName("address")
    private String address;

    @SerializedName("phone_no")
    private String phoneNo;

    @SerializedName("profile_image")
    private String profileUrl;

    @SerializedName("cover_image")
    private String coverUrl;

    @SerializedName("email")
    private String email;


    public int getUserId() {
        return userId;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public String getEmail() {
        return email;
    }

    public LoginVO(int userId, String name, String address, String phoneNo, String email, String profileUrl, String coverUrl) {
        this.userId = userId;
        this.name = name;
        this.address = address;
        this.phoneNo = phoneNo;
        this.email = email;
        this.profileUrl = profileUrl;
        this.coverUrl = coverUrl;
    }
}
