package com.padcmyanmar.fun3.pcl.event;

import com.padcmyanmar.fun3.pcl.data.vo.ChildLearningVO;

import java.util.List;

/**
 * Created by daewichan on 3/4/18.
 */

public class LoadedChildLearningEvent {
    private List<ChildLearningVO> mChildLearningList;

    public LoadedChildLearningEvent(List<ChildLearningVO> childLearningList ){
       this.mChildLearningList=childLearningList;
    }

    public List<ChildLearningVO> getChildLearningList(){
        return mChildLearningList;
    }
}
