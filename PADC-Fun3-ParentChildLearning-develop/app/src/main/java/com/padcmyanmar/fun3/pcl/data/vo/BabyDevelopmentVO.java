package com.padcmyanmar.fun3.pcl.data.vo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by htoo on 3/21/2018.
 */

public class BabyDevelopmentVO {
    @SerializedName("login_user_id")
    private int loginUserId;

    @SerializedName("actual_weight")
    private double actualWeight;

    @SerializedName("actual_height")
    private double actualHeight;

    @SerializedName("suggestion_weight")
    private double suggestionWeight;

    @SerializedName("suggestion_height")
    private double suggestionHeight;

    private String date;

    @SerializedName("other_suggestions")
    private List<OtherSuggestionVO> otherSuggestion;

    public int getLoginUserId() {
        return loginUserId;
    }

    public double getActualWeight() {
        return actualWeight;
    }

    public double getActualHeight() {
        return actualHeight;
    }

    public double getSuggestionWeight() {
        return suggestionWeight;
    }

    public double getSuggestionHeight() {
        return suggestionHeight;
    }

    public String getDate() {
        return date;
    }

    public List<OtherSuggestionVO> getOtherSuggestion() {
        return otherSuggestion;
    }

    public void setLoginUserId(int loginUserId) {
        this.loginUserId = loginUserId;
    }

    public void setActualWeight(double actualWeight) {
        this.actualWeight = actualWeight;
    }

    public void setActualHeight(double actualHeight) {
        this.actualHeight = actualHeight;
    }

    public void setSuggestionWeight(double suggestionWeight) {
        this.suggestionWeight = suggestionWeight;
    }

    public void setSuggestionHeight(double suggestionHeight) {
        this.suggestionHeight = suggestionHeight;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setOtherSuggestion(List<OtherSuggestionVO> otherSuggestion) {
        this.otherSuggestion = otherSuggestion;
    }
}
