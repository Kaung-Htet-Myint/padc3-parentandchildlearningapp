package com.padcmyanmar.fun3.pcl.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.padcmyanmar.fun3.pcl.PCLApp;
import com.padcmyanmar.fun3.pcl.R;
import com.padcmyanmar.fun3.pcl.adapters.NutritionForBabiesAdapter;
import com.padcmyanmar.fun3.pcl.data.model.ChildLearningModel;
import com.padcmyanmar.fun3.pcl.data.model.ParentLearningModel;
import com.padcmyanmar.fun3.pcl.data.vo.ParentLearningVO;
import com.padcmyanmar.fun3.pcl.delegates.ParentLearningActionDelegates;
import com.padcmyanmar.fun3.pcl.event.LoadedChildLearningEvent;
import com.padcmyanmar.fun3.pcl.event.LoadedParentLearningEvent;

import net.aungpyaephyo.mmtextview.components.MMProgressDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by User on 2/20/2018.
 */

public class NutritionForBabiesFragment extends BaseFragment {

    @BindView(R.id.rv_nutrition_for_baby)
    RecyclerView rvNutritionForBaby;
    private ParentLearningActionDelegates mParentLearningActionDelegates;
    private NutritionForBabiesAdapter mNutritionForBabiesAdapter;

    List<ParentLearningVO> mParentLearningList;

    MMProgressDialog mmProgressDialog;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mParentLearningActionDelegates = (ParentLearningActionDelegates)context;

        mParentLearningList = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_nutrition_for_baby,container,false);
        ButterKnife.bind(this,view);

        mNutritionForBabiesAdapter = new NutritionForBabiesAdapter(mParentLearningActionDelegates);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL,
                                                                                            false);
        rvNutritionForBaby.setLayoutManager(linearLayoutManager);
        rvNutritionForBaby.setAdapter(mNutritionForBabiesAdapter);

//        ParentLearningModel.getsObjectInstance().loadParentLearning();
    //    ChildLearningModel.getsObjectInstance().loadLearning();

        mmProgressDialog = new MMProgressDialog(getContext());
        mmProgressDialog.setMessage("Please wait while data is loading");
        mmProgressDialog.show();

        mParentLearningList = ParentLearningModel.getsObjectInstance().getmParentLearningList();
        if(!mParentLearningList.isEmpty()){
            mmProgressDialog.dismiss();
            List<ParentLearningVO> parentLearning = new ArrayList<>();
            for (int i=0; i<mParentLearningList.size(); i++){
                if(mParentLearningList.get(i).getType() == 3){
                    parentLearning.add(mParentLearningList.get(i));
                }
            }
            mNutritionForBabiesAdapter.setData(parentLearning);
        } else {
            ParentLearningModel.getsObjectInstance().loadParentLearning();
        }

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNutritionDataLoaded(LoadedParentLearningEvent event)
    {
        Log.d(PCLApp.LOG_TAG, "onNewsLoaded :"+event.getParentLearningList().size());
        mmProgressDialog.dismiss();
        for (int i=0; i<event.getParentLearningList().size(); i++){
            if(event.getParentLearningList().get(i).getType() == 3){
                mParentLearningList.add(event.getParentLearningList().get(i));
            }
        }
        mNutritionForBabiesAdapter.setData(mParentLearningList);
    }
}
