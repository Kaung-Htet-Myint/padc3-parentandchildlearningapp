package com.padcmyanmar.fun3.pcl.data.vo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by htoo on 3/18/2018.
 */

public class CurrentWeightAndHeightVO {
    @SerializedName("current_weight")
    private double currentWeight;

    @SerializedName("current_height")
    private double currentHeight;

    public double getCurrentWeight() {
        return currentWeight;
    }

    public double getCurrentHeight() {
        return currentHeight;
    }

    public void setCurrentWeight(double currentWeight) {
        this.currentWeight = currentWeight;
    }

    public void setCurrentHeight(double currentHeight) {
        this.currentHeight = currentHeight;
    }
}
