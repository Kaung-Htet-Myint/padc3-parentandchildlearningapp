package com.padcmyanmar.fun3.pcl.network.responses;

import com.google.gson.annotations.SerializedName;
import com.padcmyanmar.fun3.pcl.data.vo.LoginVO;

/**
 * Created by User on 3/8/2018.
 */

public class RegisterResponse {
    private int code;
    private String message;

    @SerializedName("login_user")
    private LoginVO loginUser;

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public LoginVO getLoginUser() {
        return loginUser;
    }

}
