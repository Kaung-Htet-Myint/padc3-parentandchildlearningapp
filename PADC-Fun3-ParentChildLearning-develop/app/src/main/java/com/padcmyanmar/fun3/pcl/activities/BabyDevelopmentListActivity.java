package com.padcmyanmar.fun3.pcl.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.padcmyanmar.fun3.pcl.PCLApp;
import com.padcmyanmar.fun3.pcl.R;
import com.padcmyanmar.fun3.pcl.adapters.ItemBabyDevelopmentListAdapter;
import com.padcmyanmar.fun3.pcl.data.model.BabyDevelopmentsModel;
import com.padcmyanmar.fun3.pcl.data.vo.BabyDevelopmentVO;
import com.padcmyanmar.fun3.pcl.data.vo.OtherSuggestionVO;
import com.padcmyanmar.fun3.pcl.delegates.BabyDevelopmentDelegate;
import com.padcmyanmar.fun3.pcl.dialogs.BabyDevelopmentSuggestionDialog;
import com.padcmyanmar.fun3.pcl.event.BabyDevelopmentsEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by htoo on 2/20/2018.
 */

public class BabyDevelopmentListActivity extends BaseActivity implements BabyDevelopmentDelegate {

    @BindView(R.id.rv_baby_developments)
    RecyclerView rvBabyDevelopment;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private ProgressDialog dialog;
    private List<BabyDevelopmentVO> babyDevelopmentList;

    private ItemBabyDevelopmentListAdapter mItemBabyDvelopmentAdapter;

    public static Intent newIntent(Context context){
        Intent intent = new Intent(context, BabyDevelopmentListActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_baby_development_list);

        ButterKnife.bind(this,this);

        mItemBabyDvelopmentAdapter = new ItemBabyDevelopmentListAdapter(this);

        addToolbar();

//        BabyDevelopmentsModel.getsObjInstance().loadBabyDevelopments();

        rvBabyDevelopment.setAdapter(mItemBabyDvelopmentAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext(),
                LinearLayoutManager.VERTICAL,false);
        rvBabyDevelopment.setLayoutManager(linearLayoutManager);

        rvBabyDevelopment.setVisibility(View.INVISIBLE);
        dialog = new ProgressDialog(this);
        dialog.setMessage("Please wait while data is loading");
        dialog.show();

        babyDevelopmentList = BabyDevelopmentsModel.getsObjInstance().getmBabyDevelopmentList();
        if (babyDevelopmentList == null || babyDevelopmentList.size() == 0){
            BabyDevelopmentsModel.getsObjInstance().loadBabyDevelopments();
        } else {
            addBabyDevelopmentData();
        }

    }

    private void addBabyDevelopmentData() {
        dialog.dismiss();
        rvBabyDevelopment.setVisibility(View.VISIBLE);
        if (babyDevelopmentList != null){
            mItemBabyDvelopmentAdapter.setBabyDevelopmentData(babyDevelopmentList);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    private void addToolbar() {
        setSupportActionBar(toolbar);

        if(getSupportActionBar() != null){
            getSupportActionBar().setTitle(R.string.baby_development_list_title);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_24dp);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @OnClick(R.id.fab)
    public void onTapBabyDevelopment(View view){
        Intent intent = BabyDevelopmentActivity.newIntent(getApplicationContext());
        startActivity(intent);
    }

    @Override
    public void onTapBabyDevelopmentItem(BabyDevelopmentVO babyDevelopment) {
        BabyDevelopmentSuggestionDialog dialog = new BabyDevelopmentSuggestionDialog(this,babyDevelopment);
        dialog.show();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBabyDevelopmentsLoaded(BabyDevelopmentsEvent event){
        dialog.dismiss();
        babyDevelopmentList = event.getBabyDevelopmentList();
        rvBabyDevelopment.setVisibility(View.VISIBLE);
        if (event.getBabyDevelopmentList() != null){
            mItemBabyDvelopmentAdapter.setBabyDevelopmentData(event.getBabyDevelopmentList());
        }
    }
}
