package com.padcmyanmar.fun3.pcl.fragments;

import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.padcmyanmar.fun3.pcl.R;
import com.padcmyanmar.fun3.pcl.data.model.ChildLearningModel;
import com.padcmyanmar.fun3.pcl.data.vo.ChildLearningVO;
import com.padcmyanmar.fun3.pcl.event.LoadedChildLearningEvent;

import net.aungpyaephyo.mmtextview.components.MMProgressDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by User on 2/19/2018.
 */

public class ShapeCircleFragment extends BaseFragment implements TextToSpeech.OnInitListener {

    /*@BindView(R.id.vp_shape_circle)
    ViewPager vpShapeCircle;*/

    @BindView(R.id.iv_next_btn)
    ImageView ivNextBtn;

    @BindView(R.id.iv_play_btn)
    ImageView ivPlayBtn;

    @BindView(R.id.iv_previous_btn)
    ImageView ivPreviousBtn;

    @BindView(R.id.iv_shape_type)
    ImageView ivShapeType;

    @BindView(R.id.tv_shape_type)
    TextView tvShapeType;

    @BindView(R.id.rl_shapes)
    RelativeLayout rlShapes;

    private static ShapeCircleFragment sObjInstance;
    List<ChildLearningVO> mChildLearningList;

    private TextToSpeech textToSpeech;

    ArrayList<String> imgList;
    ArrayList<String> textList;

    MMProgressDialog mmProgressDialog;

    private int[] imgArr={R.drawable.ic_brightness_104dp,R.drawable.square_img,R.drawable.triangle_img,R.drawable.ic_favorite_24dp};
    private String[] stringArr={"Circle","Square","Triangle","Heart"};


    private int curIndex = 0;

    public static ShapeCircleFragment newInstance() {
        if (sObjInstance == null) {
            sObjInstance = new ShapeCircleFragment();
        }
        return sObjInstance;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shape_circle,container,false);
        ButterKnife.bind(this,view);

        rlShapes.setVisibility(View.INVISIBLE);
//        ChildLearningModel.getsObjectInstance().loadLearning();

       /* imgList = new ArrayList<>();
        textList = new ArrayList<>();*/

        textToSpeech = new TextToSpeech(getActivity(), this);

        mmProgressDialog = new MMProgressDialog(getActivity());
        mmProgressDialog.setMessage("Please wait while data is loading");
        mmProgressDialog.show();

        mChildLearningList = ChildLearningModel.getsObjectInstance().getChildLearningList();
        mmProgressDialog.dismiss();


        imgList = new ArrayList<>();
        textList = new ArrayList<>();
        if(mChildLearningList != null)
        {
            rlShapes.setVisibility(View.VISIBLE);
            hidePreviousArrow();

            for (int i=0; i<mChildLearningList.size(); i++){

                if(mChildLearningList.get(i).getType() == 2){
                    imgList.add(mChildLearningList.get(i).getImageUrl());
                    textList.add(mChildLearningList.get(i).getText());
                }
            }
        }else {
            Snackbar.make(rlShapes,"Please check your internet connection",Snackbar.LENGTH_INDEFINITE).show();
        }

        return view;
    }

    private void showPreviousArrow()
    {
        ivPreviousBtn.setVisibility(View.VISIBLE);
    }

    private void hidePreviousArrow()
    {
        ivPreviousBtn.setVisibility(View.INVISIBLE);
    }

    private void showNextArrow(){
        ivNextBtn.setVisibility(View.VISIBLE);
    }

    private void hideNextArrow(){
        ivNextBtn.setVisibility(View.INVISIBLE);
    }

    @OnClick(R.id.rl_play_btn)
    public void onTapPlayButton(View view)
    {
        speakOut();
    }

    private void speakOut() {
        String text = tvShapeType.getText().toString();
        textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null);
    }

    @OnClick(R.id.iv_next_btn)
    public void onTapNextButton(View view)
    {
        if(curIndex < imgList.size()){

         //   ivShapeType.setImageDrawable(ivShapeType.getContext().getResources().getDrawable(imgArr[curIndex]));
            Glide.with(ivShapeType.getContext())
                    .load(imgList.get(curIndex))
                    .into(ivShapeType);

            tvShapeType.setText(textList.get(curIndex));

            //      speakOut();

            showPreviousArrow();

            curIndex++;
        }else
        {
            hideNextArrow();
        }
    }

    @OnClick(R.id.iv_previous_btn)
    public void onClickBack(View view){
        if(curIndex != 0){
            curIndex--;

           // ivShapeType.setImageDrawable(ivShapeType.getContext().getResources().getDrawable(imgArr[curIndex]));
            Glide.with(ivShapeType.getContext())
                    .load(imgList.get(curIndex))
                    .into(ivShapeType);

            tvShapeType.setText(textList.get(curIndex));
            tvShapeType.setText(textList.get(curIndex));

            //       speakOut();

            showNextArrow();
        }else{
            hidePreviousArrow();
        }
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {

            int result = textToSpeech.setLanguage(Locale.US);

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported");
            } else {
                ivPreviousBtn.setEnabled(true);
                //      speakOut();
            }

        } else {
            Log.e("TTS", "Initilization Failed!");
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onShapesDataLoad(LoadedChildLearningEvent event)
    {
//        Log.d(PCLApp.LOG_TAG, "onShapesLoaded :"+event.getChildLearningList().size());
        mmProgressDialog.dismiss();
        rlShapes.setVisibility(View.VISIBLE);
        hidePreviousArrow();
        imgList = new ArrayList<>();
        textList = new ArrayList<>();
        if(event.getChildLearningList() != null)
        {
            for (int i=0; i<event.getChildLearningList().size(); i++){

                if(event.getChildLearningList().get(i).getType() == 2){
                    imgList.add(event.getChildLearningList().get(i).getImageUrl());
                    textList.add(event.getChildLearningList().get(i).getText());
                }
            }
        }else {
            Snackbar.make(rlShapes,"Please check your internet connection",Snackbar.LENGTH_INDEFINITE).show();
        }

    }

}
