package com.padcmyanmar.fun3.pcl.delegates;

/**
 * Created by htoo on 2/19/2018.
 */

public interface BeforeLoginDelegate {

    void onTapToLogin();
    void onTapRegister();
}
