package com.padcmyanmar.fun3.pcl.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;

import com.padcmyanmar.fun3.pcl.R;
import com.padcmyanmar.fun3.pcl.adapters.StudentProfilesAdapter;
import com.padcmyanmar.fun3.pcl.components.ZoomOutPageTransformer;
import com.padcmyanmar.fun3.pcl.data.model.ParentLearningModel;
import com.padcmyanmar.fun3.pcl.data.vo.StudentProfileVO;
import com.padcmyanmar.fun3.pcl.event.StudentProfilesLoadedEvent;
import com.padcmyanmar.fun3.pcl.fragments.StudentProfileFragment;
import com.rd.PageIndicatorView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StudentProfilesActivity extends BaseActivity {

    @BindView(R.id.vp_student_profiles)
    ViewPager vpStudentProfiles;

    @BindView(R.id.pi_student_profiles)
    PageIndicatorView piStudentProfiles;

    private StudentProfilesAdapter mStudentProfilesAdapter;

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, StudentProfilesActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_students_profile);
        ButterKnife.bind(this, this);

        mStudentProfilesAdapter = new StudentProfilesAdapter(getSupportFragmentManager());

        List<StudentProfileVO> studentProfiles = ParentLearningModel.getsObjectInstance().getStudentProfiles();
        if (!studentProfiles.isEmpty()) {
            for (StudentProfileVO studentProfile : studentProfiles) {
                mStudentProfilesAdapter.addTab(studentProfile.getName(), StudentProfileFragment.newInstance(studentProfile.getPhoneNo()));
            }
            piStudentProfiles.setCount(mStudentProfilesAdapter.getCount());
        }

        vpStudentProfiles.setAdapter(mStudentProfilesAdapter);
        vpStudentProfiles.setPageTransformer(true, new ZoomOutPageTransformer());

        vpStudentProfiles.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                piStudentProfiles.setSelection(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onStudentProfilesLoaded(StudentProfilesLoadedEvent event) {
        if (!event.getStudentProfiles().isEmpty()) {
            for (StudentProfileVO studentProfile : event.getStudentProfiles()) {
                mStudentProfilesAdapter.addTab(studentProfile.getName(), StudentProfileFragment.newInstance(studentProfile.getPhoneNo()));
            }
            vpStudentProfiles.setAdapter(mStudentProfilesAdapter);
            mStudentProfilesAdapter.notifyDataSetChanged();
            piStudentProfiles.setCount(mStudentProfilesAdapter.getCount());
        }
    }
}
