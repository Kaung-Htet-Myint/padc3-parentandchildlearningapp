package com.padcmyanmar.fun3.pcl.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.padcmyanmar.fun3.pcl.PCLApp;
import com.padcmyanmar.fun3.pcl.R;
import com.padcmyanmar.fun3.pcl.data.model.ChildLearningModel;
import com.padcmyanmar.fun3.pcl.data.vo.ChildLearningVO;
import com.padcmyanmar.fun3.pcl.event.LoadedChildLearningEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by htoo on 2/20/2018.
 */

public class AnimalsGameFragment extends BaseFragment implements TextToSpeech.OnInitListener {

    @BindView(R.id.btn_previous)
    ImageView btnPrevious;
    @BindView(R.id.btn_play)
    ImageView btnPlay;
    @BindView(R.id.btn_next)
    ImageView btnNext;
    @BindView(R.id.iv_animal)
    ImageView ivAnimal;
    @BindView(R.id.tv_animal)
    TextView tvAnimal;
    @BindView(R.id.rl_fragment_animal)
    RelativeLayout rlFragmentAnimal;

    private TextToSpeech speech;
    private static AnimalsGameFragment animalsGameFragment;

    ArrayList<String> animalImage;
    ArrayList<String> animalAlpha;

    private List<ChildLearningVO> childLearningList;

    private ProgressDialog mProgressDialog;

    private int index;

    private static AnimalsGameFragment instance(){
        if(animalsGameFragment == null){
            animalsGameFragment = new AnimalsGameFragment();
        }
        return animalsGameFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_animals_game,container,false);
        ButterKnife.bind(this,view);

        rlFragmentAnimal.setVisibility(View.INVISIBLE);

//        ChildLearningModel.getsObjectInstance().loadLearning();

        speech = new TextToSpeech(getActivity(), this);

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Please wait while data is loading");
        mProgressDialog.show();

        hideBackArrow();

        index = 0;

        childLearningList = ChildLearningModel.getsObjectInstance().getChildLearningList();
        mProgressDialog.dismiss();

        animalImage = new ArrayList<>();
        animalAlpha = new ArrayList<>();

        if (childLearningList.size() != 0){
            try {
                if(!childLearningList.isEmpty() || childLearningList != null){
                    rlFragmentAnimal.setVisibility(View.VISIBLE);
                    hideBackArrow();

                    for (int i=0; i<childLearningList.size(); i++){
                        if(childLearningList.get(i).getType() == 1) {
                            animalImage.add(childLearningList.get(i).getImageUrl());
                            animalAlpha.add(childLearningList.get(i).getText());
                        }
                    }

                    Glide.with(ivAnimal.getContext())
                            .load(animalImage.get(index))
                            .into(ivAnimal);
                    tvAnimal.setText(animalAlpha.get(index));
                }
            } catch (NullPointerException e){
                Snackbar.make(rlFragmentAnimal, "Please check your internet connection", Snackbar.LENGTH_INDEFINITE).show();
            }
        } else {
            ChildLearningModel.getsObjectInstance().loadLearning();
        }

        return view;
    }

    private void showBackArrow() {
        btnPrevious.setVisibility(View.VISIBLE);
    }

    private void hideBackArrow() {
        btnPrevious.setVisibility(View.INVISIBLE);
    }

    private void showForwardArrow() {
        btnNext.setVisibility(View.VISIBLE);
    }

    private void hideForwardArrow() {
        btnNext.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {

            int result = speech.setLanguage(Locale.US);

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e(PCLApp.LOG_TAG, "This Language is not supported");
            }

        } else {
            Log.e(PCLApp.LOG_TAG, "Initilization Failed!");
        }
    }

    @OnClick(R.id.iv_animal)
    public void onTapAnimal(View view){
        speakOut();
    }

    @OnClick(R.id.btn_next)
    public void onClickNext(View view){
        if(index<animalImage.size()){
            Glide.with(ivAnimal.getContext())
                    .load(animalImage.get(index))
                    .into(ivAnimal);
            tvAnimal.setText(animalAlpha.get(index));

            showBackArrow();

            index++;
        }else{
            hideForwardArrow();
        }
    }

    @OnClick(R.id.btn_previous)
    public void onClickPrevious(View view){
        if(index != 0){
            index--;

            Glide.with(ivAnimal.getContext())
                    .load(animalImage.get(index))
                    .into(ivAnimal);
            tvAnimal.setText(animalAlpha.get(index));

            showForwardArrow();
        }else{
            hideBackArrow();
        }
    }


    private void speakOut() {
        String text = tvAnimal.getText().toString();
        speech.speak(text, TextToSpeech.QUEUE_FLUSH, null);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAnimalGameLoaded(LoadedChildLearningEvent event){
//        Log.d(PCLApp.LOG_TAG, "onAnimalGameLoaded : " + event.getChildLearningList().size());
        mProgressDialog.dismiss();

        animalImage = new ArrayList<>();
        animalAlpha = new ArrayList<>();

        try {
            if(!event.getChildLearningList().isEmpty() || event.getChildLearningList() != null){
                rlFragmentAnimal.setVisibility(View.VISIBLE);
                hideBackArrow();

                for (int i=0; i<event.getChildLearningList().size(); i++){
                    if(event.getChildLearningList().get(i).getType() == 1) {
                        animalImage.add(event.getChildLearningList().get(i).getImageUrl());
                        animalAlpha.add(event.getChildLearningList().get(i).getText());
                    }
                }

                Glide.with(ivAnimal.getContext())
                        .load(animalImage.get(index))
                        .into(ivAnimal);
                tvAnimal.setText(animalAlpha.get(index));
            }
        } catch (NullPointerException e){
            Snackbar.make(rlFragmentAnimal, "Please check your internet connection", Snackbar.LENGTH_INDEFINITE).show();
        }
    }
}
