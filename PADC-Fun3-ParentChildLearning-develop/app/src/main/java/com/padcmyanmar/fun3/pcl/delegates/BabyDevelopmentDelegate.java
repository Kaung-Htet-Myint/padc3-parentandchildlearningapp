package com.padcmyanmar.fun3.pcl.delegates;

import com.padcmyanmar.fun3.pcl.data.vo.BabyDevelopmentVO;

/**
 * Created by htoo on 2/22/2018.
 */

public interface BabyDevelopmentDelegate {
    void onTapBabyDevelopmentItem(BabyDevelopmentVO babyDevelopment);
}
