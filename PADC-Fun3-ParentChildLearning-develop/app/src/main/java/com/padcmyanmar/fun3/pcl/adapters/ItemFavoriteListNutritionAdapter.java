package com.padcmyanmar.fun3.pcl.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.padcmyanmar.fun3.pcl.R;
import com.padcmyanmar.fun3.pcl.data.vo.ParentLearningVO;
import com.padcmyanmar.fun3.pcl.delegates.FavoriteDelegate;
import com.padcmyanmar.fun3.pcl.viewholders.ItemFavoriteListNutritionViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by htoo on 3/14/2018.
 */

public class ItemFavoriteListNutritionAdapter extends RecyclerView.Adapter<ItemFavoriteListNutritionViewHolder> {

    private FavoriteDelegate mFavoriteDelegate;
    private List<ParentLearningVO> mFavoriteNutritionList;

    public ItemFavoriteListNutritionAdapter(FavoriteDelegate favoriteDelegate) {
        mFavoriteNutritionList = new ArrayList<>();

        mFavoriteDelegate = favoriteDelegate;
    }

    @Override
    public ItemFavoriteListNutritionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.item_favorite_list_nutrition, parent, false);
        ItemFavoriteListNutritionViewHolder viewHolder = new ItemFavoriteListNutritionViewHolder(view, mFavoriteDelegate);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ItemFavoriteListNutritionViewHolder holder, int position) {
        holder.setData(mFavoriteNutritionList.get(position));
    }

    @Override
    public int getItemCount() {
        return mFavoriteNutritionList.size();
    }

    public void setFavoriteNutritionData(List<ParentLearningVO> favoriteNutritionList){
        mFavoriteNutritionList = favoriteNutritionList;
        notifyDataSetChanged();
    }
}
