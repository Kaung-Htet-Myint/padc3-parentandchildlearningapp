package com.padcmyanmar.fun3.pcl.data.vo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by htoo on 3/18/2018.
 */

public class SuggestionVO {

    @SerializedName("suggestion_weight")
    private int suggestionWeight;

    @SerializedName("suggestion_height")
    private int suggestionHeight;

    @SerializedName("other_suggestions")
    private List<OtherSuggestionVO> otherSuggestionList;

    public int getSuggestionWeight() {
        return suggestionWeight;
    }

    public int getIntsuggestionHeight() {
        return suggestionHeight;
    }

    public List<OtherSuggestionVO> getOtherSuggestionList() {
        return otherSuggestionList;
    }
}
