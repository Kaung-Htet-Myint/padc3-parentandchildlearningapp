package com.padcmyanmar.fun3.pcl.network.responses;

import com.google.gson.annotations.SerializedName;
import com.padcmyanmar.fun3.pcl.data.vo.CurrentWeightAndHeightVO;
import com.padcmyanmar.fun3.pcl.data.vo.SuggestionVO;

/**
 * Created by htoo on 3/20/2018.
 */

public class AddBabyDevelopmentResponse {
    private int code;
    private String message;
    private String apiVersion;

    @SerializedName("babyInfo")
    private CurrentWeightAndHeightVO babyDevelopment;

    private SuggestionVO suggestion;

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String getApiVersion() {
        return apiVersion;
    }

    public CurrentWeightAndHeightVO getBabyDevelopment() {
        return babyDevelopment;
    }

    public SuggestionVO getSuggestion() {
        return suggestion;
    }
}
