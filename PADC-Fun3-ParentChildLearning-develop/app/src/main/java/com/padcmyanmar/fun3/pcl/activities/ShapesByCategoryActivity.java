package com.padcmyanmar.fun3.pcl.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;

import com.padcmyanmar.fun3.pcl.R;
import com.padcmyanmar.fun3.pcl.adapters.ShapesByCategoryAdapter;
import com.padcmyanmar.fun3.pcl.fragments.ShapeCircleFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by User on 2/19/2018.
 */

public class ShapesByCategoryActivity extends BaseActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.vp_shapes_by_category)
    ViewPager vpShapesByCategory;

    @BindView(R.id.tb_shapes_by_category)
    TabLayout tbShapesByCategory;

    private ShapesByCategoryAdapter mShapesByCategoryAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shapes_by_category);
        ButterKnife.bind(this,this);

        setSupportActionBar(toolbar);

        if(getSupportActionBar() != null)
        {
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_24dp);
            getSupportActionBar().setTitle("Challenges");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mShapesByCategoryAdapter = new ShapesByCategoryAdapter(getSupportFragmentManager());
        vpShapesByCategory.setAdapter(mShapesByCategoryAdapter);

        mShapesByCategoryAdapter.addTab("Shapes",new ShapeCircleFragment());

        tbShapesByCategory.setupWithViewPager(vpShapesByCategory);

        vpShapesByCategory.setOffscreenPageLimit(mShapesByCategoryAdapter.getCount());
    }


  }
