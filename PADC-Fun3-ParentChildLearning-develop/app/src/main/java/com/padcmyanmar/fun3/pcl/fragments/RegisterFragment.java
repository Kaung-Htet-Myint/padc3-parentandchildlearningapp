package com.padcmyanmar.fun3.pcl.fragments;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.padcmyanmar.fun3.pcl.R;
import com.padcmyanmar.fun3.pcl.data.model.RegisterUserModel;
import com.padcmyanmar.fun3.pcl.network.responses.RegisterResponse;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by daewichan on 2/19/18.
 */

public class RegisterFragment extends Fragment {

    @BindView(R.id.tv_already)
    TextView tvAlready;

    @BindView(R.id.et_name)
    EditText etName;

    @BindView(R.id.et_phone_no)
    EditText etPhone;

    @BindView(R.id.et_date_of_birth)
    EditText etDateOfBirth;

    @BindView(R.id.et_country)
    EditText etCountry;

    @BindView(R.id.et_password)
    EditText etPassword;

    private String strName = "", strPhoneNumber = "", strDob = "", strCountry = "", strPassword = "";

    private static RegisterFragment sObjectInstance;
    private ProgressDialog progressDialog;

    Calendar calendar = Calendar.getInstance();


    public static RegisterFragment newInstance() {
        if (sObjectInstance == null) {
            sObjectInstance = new RegisterFragment();
        }
        return sObjectInstance;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register, container, false);

        ButterKnife.bind(this, view);
        return view;
    }


    DatePickerDialog.OnDateSetListener dateOfBirth = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            String myFormat = "MM/dd/yy"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

            etDateOfBirth.setText(sdf.format(calendar.getTime()));

        }
    };

    @OnClick(R.id.et_date_of_birth)
    public void onClickDate(View view) {
        new DatePickerDialog(getActivity(), dateOfBirth, calendar
                .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)).show();
    }


    @OnClick(R.id.tv_already)
    public void onTapAlready(View view) {
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.fl_container, new LoginFragment())
                .commit();
    }

    private void getInputData() {
        strName = etName.getText().toString();
        strPhoneNumber = etPhone.getText().toString();
        strDob = etDateOfBirth.getText().toString();
        strCountry = etCountry.getText().toString();
        strPassword = etPassword.getText().toString();
    }

    private boolean hasInputData() {

        if (TextUtils.isEmpty(strName)) {
            etName.setError("Enter user name");
            return false;
        }


        if (TextUtils.isEmpty(strPhoneNumber)) {
            etPhone.setError("Enter phone number");
            return false;
        }

        if (TextUtils.isEmpty(strDob)) {
            etDateOfBirth.setError("Enter date of birth");
            return false;
        }

        if (TextUtils.isEmpty(strCountry)) {
            etCountry.setError("Enter country name");
            return false;
        }

        if (TextUtils.isEmpty(strPassword)) {
            etPassword.setError("Enter password");
            return false;
        }

        return true;
    }

    @OnClick(R.id.btn_register)
    public void onFillInfo(View view) {
        getInputData();

        if (hasInputData()) {

            progressDialog = new ProgressDialog(getActivity());
            //progressDialog.setCancelable(false);
            progressDialog.setMessage("Please wait while data loading");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
            progressDialog.setCancelable(false);
            RegisterUserModel.getsObjInstance(getContext()).registerUser(strName, strPhoneNumber, strDob, strCountry, strPassword);
        } else {
            Toast.makeText(getContext(), "Please fill all the require field!", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoginUserSucess(RegisterResponse registerResponse) {
        if (getActivity() != null) {

            progressDialog.dismiss();
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fl_container, new LoginFragment())
                    .commit();

            Toast.makeText(getContext(), registerResponse.getMessage(), Toast.LENGTH_SHORT).show();
            //getActivity().onBackPressed();
        }
    }

}
