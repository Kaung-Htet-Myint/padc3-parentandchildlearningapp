package com.padcmyanmar.fun3.pcl.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.padcmyanmar.fun3.pcl.R;
import com.padcmyanmar.fun3.pcl.viewitems.ImageInNutritionForBabayDetailsViewItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 2/21/2018.
 */

public class ImageInNutritionForBabiesDetailsAdapter extends PagerAdapter{

    private List<String> mImages;

    public ImageInNutritionForBabiesDetailsAdapter(List<String> imgUrlList) {
        mImages = new ArrayList<>();
    }

    public int getCount() {
        return mImages.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return (view == (View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Context context = container.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        ImageInNutritionForBabayDetailsViewItem view = (ImageInNutritionForBabayDetailsViewItem)
                                                        layoutInflater.inflate
                                                        (R.layout.item_nutrition_for_baby_details_image,container,false);

       view.setData(mImages.get(position));
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    public void setImages(List<String> imgUrlList){
        mImages = imgUrlList;
        notifyDataSetChanged();
    }
}
