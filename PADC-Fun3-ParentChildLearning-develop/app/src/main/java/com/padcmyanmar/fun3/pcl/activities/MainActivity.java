package com.padcmyanmar.fun3.pcl.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.padcmyanmar.fun3.pcl.R;
import com.padcmyanmar.fun3.pcl.adapters.DoAndDonotAdapter;
import com.padcmyanmar.fun3.pcl.adapters.ParentAdapter;
import com.padcmyanmar.fun3.pcl.adapters.ParentCategoryAdapter;
import com.padcmyanmar.fun3.pcl.data.model.LoginUserModel;
import com.padcmyanmar.fun3.pcl.data.model.ParentLearningModel;
import com.padcmyanmar.fun3.pcl.data.vo.CurrentWeightAndHeightVO;
import com.padcmyanmar.fun3.pcl.data.vo.LoginVO;
import com.padcmyanmar.fun3.pcl.data.vo.ParentLearningVO;
import com.padcmyanmar.fun3.pcl.delegates.BeforeLoginDelegate;
import com.padcmyanmar.fun3.pcl.delegates.LoginUserDelegate;
import com.padcmyanmar.fun3.pcl.delegates.ParentLearningActionDelegates;
import com.padcmyanmar.fun3.pcl.fragments.DoAndDonotFragment;
import com.padcmyanmar.fun3.pcl.fragments.LearningTipsFragment;
import com.padcmyanmar.fun3.pcl.fragments.NutritionForBabiesFragment;
import com.padcmyanmar.fun3.pcl.viewpods.AccountControlViewPod;
import com.padcmyanmar.fun3.pcl.viewpods.BeforeLoginViewPod;
import com.padcmyanmar.fun3.pcl.viewpods.LoginUserViewPod;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity
        implements BeforeLoginDelegate, ParentLearningActionDelegates, LoginUserDelegate {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    /*@BindView(R.id.fab)
    FloatingActionButton fab;*/

    private ParentAdapter mParentAdapter;
    private DoAndDonotAdapter mDoAndDonotAdapter;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @BindView(R.id.navigation_view)
    NavigationView navigation;

    @BindView(R.id.vp_parent_category)
    ViewPager vpMenuParents;

    @BindView(R.id.tl_parent_category)
    TabLayout tlParentCategory;


    private AccountControlViewPod vpAccountControlViewPod;
    private BeforeLoginViewPod vpBeforeLogin;

    private ParentCategoryAdapter mParentCategoryAdapter;

    private ParentLearningActionDelegates mActionDelegates;

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this, this);
        addToolbar();

        mParentAdapter = new ParentAdapter(getSupportFragmentManager());
        //mDoAndDonotAdapter = new DoAndDonotAdapter(this);
        vpMenuParents.setAdapter(mParentAdapter);

        tlParentCategory.setupWithViewPager(vpMenuParents);
        vpMenuParents.setOffscreenPageLimit(mParentAdapter.getCount());

       /* vpBeforeLogin = (BeforeLoginViewPod) navigation.getHeaderView(0);
        vpBeforeLogin.setDelegate(this);*/

        mParentCategoryAdapter = new ParentCategoryAdapter(getSupportFragmentManager());
        vpMenuParents.setAdapter(mParentCategoryAdapter);

        mParentCategoryAdapter.addTab("Do & Don\'t", new DoAndDonotFragment());
        mParentCategoryAdapter.addTab("Learning Tips", LearningTipsFragment.newInstance());
        mParentCategoryAdapter.addTab("Nutrition For Child", new NutritionForBabiesFragment());

        tlParentCategory.setupWithViewPager(vpMenuParents);

        navigation.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                if (item.getItemId() == R.id.menu_for_child) {
                    Intent intent = ChildCategoryActivity.newIntent(getApplicationContext());
                    startActivity(intent);
                } else if (item.getItemId() == R.id.menu_for_favorite) {
                    Intent intent = FavoriteListCategoryActivity.newIntent(getApplicationContext());
                    startActivity(intent);
                } else if (item.getItemId() == R.id.menu_student_profiles) {
                    Intent intent = StudentProfilesActivity.newIntent(getApplicationContext());
                    startActivity(intent);
                }
                drawerLayout.closeDrawer(GravityCompat.START);

                return false;
            }
        });

        vpAccountControlViewPod = (AccountControlViewPod) navigation.getHeaderView(0);
        vpAccountControlViewPod.setDelegate(this);
        vpAccountControlViewPod.setLoginUserDelegate(this);


        ParentLearningModel.getsObjectInstance().loadParentLearning();

        if (LoginUserModel.getsObjInstance(getApplicationContext()).isUserLogin(getApplicationContext())) {
            LoginVO loginVO = LoginUserModel.getsObjInstance(getApplicationContext()).getLoginUser();
            LoginUserViewPod loginUserViewPod = new LoginUserViewPod(getApplicationContext());
//            loginUserViewPod.bindData(loginVO);
//            Log.d("Login user name :", loginVO.getName());
        }

    }


    private void addToolbar() {
        setSupportActionBar(toolbar);

        // add hamburger icon to open drawer layout
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.menu_title);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu_24dp);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == android.R.id.home) {
            drawerLayout.openDrawer(GravityCompat.START);
        }

        return super.onOptionsItemSelected(item);
    }

    /*@OnClick(R.id.fab)
    public void onTapFab(View view) {
        *//*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();*//*
        startServiceComponent();
    }*/

    @Override
    public void onTapToLogin() {
        Intent intent = AccountControlActivity.newIntentLogin(getApplicationContext());
        startActivity(intent);
    }

    @Override
    public void onTapRegister() {
        Intent intent = AccountControlActivity.newIntentRegister(getApplicationContext());
        startActivity(intent);
    }

    @Override
    public void onTapNutritionItem() {

    }

    @Override
    public void onTapInfoBrief(ParentLearningVO pLearningVO) {
        Intent intent = new Intent(getApplicationContext(), NutritionForBabiesDetailsActivity.class);
        intent.putExtra("pLearning_id", pLearningVO.getId());
        startActivity(intent);
    }


    @Override
    public void onTapFavButton() {

    }

    @Override
    public void onTapCommentButton() {

    }

    @Override
    public void onTapShareButton() {

    }

    @Override
    public void onTapSaveButton() {

    }


    @Override
    public void onTapLogout() {
        LoginUserModel.getsObjInstance(getApplicationContext()).logout(getApplicationContext());
    }

    @Override
    public void onTapBabyInfo() {
        Intent intent = BabyInfoActivity.newIntent(getApplicationContext());
        startActivity(intent);
    }

    @Override
    public void onTapBayDevelopment() {
        Intent intent = BabyDevelopmentListActivity.newIntent(getApplicationContext());
        startActivity(intent);
    }

    public void addBabyDevelopment(CurrentWeightAndHeightVO babyDevelopmentVO) {

    }
}
