package com.padcmyanmar.fun3.pcl.data.vo;

import com.google.gson.annotations.SerializedName;

public class StudentProfileVO {

    @SerializedName("name")
    private String name;

    @SerializedName("contributions_to_project")
    private String contributionsToProject;

    @SerializedName("goals_in_2019")
    private String goalsIn2019;

    @SerializedName("photo")
    private String photo;

    @SerializedName("phone_no")
    private String phoneNo;

    @SerializedName("linkedin")
    private String linkedIn;

    @SerializedName("github")
    private String github;

    public String getName() {
        return name;
    }

    public String getContributionsToProject() {
        return contributionsToProject;
    }

    public String getGoalsIn2019() {
        return goalsIn2019;
    }

    public String getPhoto() {
        return photo;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public String getLinkedIn() {
        return linkedIn;
    }

    public String getGithub() {
        return github;
    }
}
