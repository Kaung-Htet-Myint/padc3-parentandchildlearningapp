package com.padcmyanmar.fun3.pcl.viewitems;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.padcmyanmar.fun3.pcl.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by User on 2/21/2018.
 */

public class ImageInNutritionForBabayDetailsViewItem extends FrameLayout{

    @BindView(R.id.iv_nutrition_for_baby_details_image)
    ImageView ivNutritionForBabyDetailsImage;

    private List<String> mImages;

    public ImageInNutritionForBabayDetailsViewItem(@NonNull Context context) {
        super(context);
    }

    public ImageInNutritionForBabayDetailsViewItem(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public ImageInNutritionForBabayDetailsViewItem(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this,this);
    }

    //to bind dynamic data
    public void setData(String imageUrl){
        Glide.with(ivNutritionForBabyDetailsImage.getContext())
                .load(imageUrl)
                .into(ivNutritionForBabyDetailsImage);
    }
}
