package com.padcmyanmar.fun3.pcl.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.padcmyanmar.fun3.pcl.R;
import com.padcmyanmar.fun3.pcl.data.vo.ParentLearningVO;
import com.padcmyanmar.fun3.pcl.delegates.FavoriteDelegate;
import com.padcmyanmar.fun3.pcl.viewholders.ItemFavoriteDoViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by htoo on 2/23/2018.
 */

public class ItemFavoriteListDoAdapter extends RecyclerView.Adapter<ItemFavoriteDoViewHolder> {

    private FavoriteDelegate mFavoriteDelegate;
    private List<ParentLearningVO> mFavoriteDoAndDonotList;

    public ItemFavoriteListDoAdapter(FavoriteDelegate favoriteDelegate){
        mFavoriteDelegate = favoriteDelegate;
        mFavoriteDoAndDonotList = new ArrayList<>();
    }

    @Override
    public ItemFavoriteDoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.item_favorite_list_do, parent, false);
        ItemFavoriteDoViewHolder viewHolder = new ItemFavoriteDoViewHolder(view, mFavoriteDelegate);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ItemFavoriteDoViewHolder holder, int position) {
        holder.setData(mFavoriteDoAndDonotList.get(position));
    }

    @Override
    public int getItemCount() {
        return mFavoriteDoAndDonotList.size();
    }

    public void setFavoriteDoAndDonotData(List<ParentLearningVO> learningList){
        mFavoriteDoAndDonotList = learningList;
        notifyDataSetChanged();

    }
}
