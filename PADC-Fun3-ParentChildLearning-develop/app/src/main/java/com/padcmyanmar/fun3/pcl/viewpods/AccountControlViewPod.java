package com.padcmyanmar.fun3.pcl.viewpods;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

import com.padcmyanmar.fun3.pcl.R;
import com.padcmyanmar.fun3.pcl.data.model.LoginUserModel;
import com.padcmyanmar.fun3.pcl.delegates.BeforeLoginDelegate;
import com.padcmyanmar.fun3.pcl.delegates.LoginUserDelegate;
import com.padcmyanmar.fun3.pcl.event.SuccessLoginEvent;
import com.padcmyanmar.fun3.pcl.event.UserLogoutEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by htoo on 2/19/2018.
 */

public class AccountControlViewPod extends FrameLayout {

    @BindView(R.id.view_pod_before_login)
    BeforeLoginViewPod vpBeforeLogin;

    @BindView(R.id.view_pod_login_user)
    LoginUserViewPod vpLoginUser;

    Context ctx;

    private LoginUserDelegate mLoginUserDelegate;

    public AccountControlViewPod(@NonNull Context context) {
        super(context);
    }

    public AccountControlViewPod(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public AccountControlViewPod(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this,this);
        refreshUserSession();
        EventBus.getDefault().register(this);
    }

    public void setDelegate(BeforeLoginDelegate beforeLoginDelegate){
        vpBeforeLogin.setDelegate(beforeLoginDelegate);
    }

    public void setLoginUserDelegate(LoginUserDelegate loginUserDelegate)
    {
        vpLoginUser.setDelegate(loginUserDelegate);
        mLoginUserDelegate = loginUserDelegate;
    }

    public void refreshUserSession(){
        if(LoginUserModel.getsObjInstance(getContext()).isUserLogin(ctx)){
            vpBeforeLogin.setVisibility(View.GONE);
            vpLoginUser.setVisibility(View.VISIBLE);
        }else {
            vpBeforeLogin.setVisibility(View.VISIBLE);
            vpLoginUser.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.btn_baby_info)
    public void onTapBabyInfo(View view)
    {
        mLoginUserDelegate.onTapBabyInfo();
    }

    @OnClick(R.id.btn_baby_development)
    public void onTapBabyDevelopment(View view)
    {
        mLoginUserDelegate.onTapBayDevelopment();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoginUserSucess(SuccessLoginEvent event){
        vpBeforeLogin.setVisibility(View.GONE);
        vpLoginUser.setVisibility(View.VISIBLE);

        vpLoginUser.bindData(event.getLogin());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLogoutUser(UserLogoutEvent event)
    {
        vpBeforeLogin.setVisibility(View.VISIBLE);
        vpLoginUser.setVisibility(View.GONE);
    }

}
