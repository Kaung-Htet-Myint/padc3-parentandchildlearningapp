package com.padcmyanmar.fun3.pcl.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.padcmyanmar.fun3.pcl.R;
import com.padcmyanmar.fun3.pcl.activities.DoAndDonotDetailActivity;
import com.padcmyanmar.fun3.pcl.adapters.DoAndDonotAdapter;
import com.padcmyanmar.fun3.pcl.adapters.DoAndDonotPopularAdapter;
import com.padcmyanmar.fun3.pcl.data.model.ParentLearningModel;
import com.padcmyanmar.fun3.pcl.data.vo.ParentLearningVO;
import com.padcmyanmar.fun3.pcl.delegates.ParentDelegate;
import com.padcmyanmar.fun3.pcl.event.LoadedParentLearningEvent;
import com.padcmyanmar.fun3.pcl.event.NetworkErrorEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by einandartun on 2/20/18.
 */

public class DoAndDonotFragment extends BaseFragment implements ParentDelegate {
    @BindView(R.id.rv_do_and_donot)
    RecyclerView rvDoAndDonot;

    @BindView(R.id.rv_most_popular)
    RecyclerView rvMostPopular;

    @BindView(R.id.tv_most_popular)
    TextView tvMostPopular;

    @BindView(R.id.tv_new_articles)
    TextView tvNewArticle;

    private DoAndDonotAdapter mDoAndDonotAdapter;
    private DoAndDonotPopularAdapter mDoAndDonotPopularAdapter;
    private ProgressDialog mProgressDialog;

    List<ParentLearningVO> mParentLearningList;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_do_and_donot,container,false);
        ButterKnife.bind(this,view);

        mDoAndDonotPopularAdapter = new DoAndDonotPopularAdapter(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        rvMostPopular.setLayoutManager(linearLayoutManager);
        rvMostPopular.setAdapter(mDoAndDonotPopularAdapter);

        mDoAndDonotAdapter = new DoAndDonotAdapter(this);
        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        rvDoAndDonot.setLayoutManager(linearLayoutManager1);
        rvDoAndDonot.setAdapter(mDoAndDonotAdapter);
//        ParentLearningModel.getsObjectInstance().loadParentLearning();

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Please wait while data is loading");
        hideTextView();
        mProgressDialog.show();

        mParentLearningList = ParentLearningModel.getsObjectInstance().getmParentLearningList();
        if(!mParentLearningList.isEmpty()){
            showTextView();
            mProgressDialog.dismiss();
            List<ParentLearningVO> mDoAndDonotList = new ArrayList<>();
            List<ParentLearningVO> mPopularList= new ArrayList<>();

            for (int i=0; i<mParentLearningList.size(); i++){
                if (mParentLearningList.get(i).getType() == 1){
                    mDoAndDonotList.add(mParentLearningList.get(i));
                }
                if (mParentLearningList.get(i).getIsMostPopular() == 1){
                    mPopularList.add(mParentLearningList.get(i));
                }
            }
            mDoAndDonotAdapter.setParentData(mDoAndDonotList);
            mDoAndDonotPopularAdapter.setPopularParentData(mPopularList);
        } else {
            ParentLearningModel.getsObjectInstance().loadParentLearning();
        }

        return view;
    }

    private void showTextView() {
        tvMostPopular.setVisibility(View.VISIBLE);
        tvNewArticle.setVisibility(View.VISIBLE);
    }

    private void hideTextView() {
        tvMostPopular.setVisibility(View.GONE);
        tvNewArticle.setVisibility(View.GONE);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onTapDoAndDonotItem(ParentLearningVO mParentLearning) {
        Intent intent = new Intent(getContext(), DoAndDonotDetailActivity.class);
        intent.putExtra("id_do_donot",String.valueOf(mParentLearning.getId()));
        startActivity(intent);
    }

    @Override
    public void onTapMostPopularItem(ParentLearningVO mParentLearning){
        Intent intent = new Intent(getContext(), DoAndDonotDetailActivity.class);
        intent.putExtra("id_do_donot",String.valueOf(mParentLearning.getId()));
        startActivity(intent);
    }

    public void networkErrorWhenDataLoad(NetworkErrorEvent event){
        Snackbar.make(tvNewArticle, "Please check your internet connection", Snackbar.LENGTH_INDEFINITE).show();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onParentLearningLoaded(LoadedParentLearningEvent event){
        mProgressDialog.dismiss();
        List<ParentLearningVO> mDoAndDonotList = new ArrayList<>();
        List<ParentLearningVO> mPopularList= new ArrayList<>();

        try {
            if (!event.getParentLearningList().isEmpty()){
                for (ParentLearningVO parentLearning :event.getParentLearningList() ){
                    if (parentLearning.getType() == 1){
                        mDoAndDonotList.add(parentLearning);
                    }
                    if (parentLearning.getIsMostPopular() == 1){
                        mPopularList.add(parentLearning);
                    }
                }
                mDoAndDonotAdapter.setParentData(mDoAndDonotList);
                mDoAndDonotPopularAdapter.setPopularParentData(mPopularList);
            }
            //mDoAndDonotAdapter.setParentData(event.getParentLearningVOList());
        } catch (NullPointerException e){
            Snackbar.make(tvNewArticle, "Please check your internet connection", Snackbar.LENGTH_INDEFINITE).show();
        }

        //mDoAndDonotPopularAdapter.setPopularParentData(event.getParentLearningVOList());
    }

//    @OnClick(R.id.fab_favorite_dodonot)
//    public void onTapFabDoANdDonot(View view){
//    Toast.makeText(view.getContext(), "On click img",Toast.LENGTH_SHORT).show();
//}
}
