package com.padcmyanmar.fun3.pcl.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

import com.padcmyanmar.fun3.pcl.PCLApp;

/**
 * Created by User on 3/9/2018.
 */

public class SampleService extends IntentService {
    private static final String IE_TIMESTAMP = "timestamp";

    public static Intent newInent(Context context, String timestamp){
        Intent intent = new Intent(context, SampleService.class);
        intent.putExtra(IE_TIMESTAMP,timestamp);
        return intent;
    }

    public SampleService() {
        super("SimpleService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        String timestamp="";
        if(intent != null){
            timestamp = intent.getStringExtra(IE_TIMESTAMP);
        }

        Log.d(PCLApp.LOG_TAG,"SampleService : onHandleIntent"+timestamp);
    }
}
