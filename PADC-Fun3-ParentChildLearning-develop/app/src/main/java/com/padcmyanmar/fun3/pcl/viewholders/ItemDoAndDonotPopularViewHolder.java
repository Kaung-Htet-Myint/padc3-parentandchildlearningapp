package com.padcmyanmar.fun3.pcl.viewholders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.padcmyanmar.fun3.pcl.R;
import com.padcmyanmar.fun3.pcl.data.vo.ParentLearningVO;
import com.padcmyanmar.fun3.pcl.delegates.ParentDelegate;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by einandartun on 2/25/18.
 */

public class ItemDoAndDonotPopularViewHolder extends BaseViewHolder {

    private ParentDelegate mParentDelegate;
    private ParentLearningVO mPopularVO;

    @BindView(R.id.tv_popular_title)
    TextView tvPopularTitle;

    @BindView(R.id.iv_most_popular)
    ImageView ivMostPopular;

    public ItemDoAndDonotPopularViewHolder(View itemView, ParentDelegate parentDelegate) {
        super(itemView);
        ButterKnife.bind(this,itemView);
        mParentDelegate = parentDelegate;
    }

    @Override
    public void setData(Object data) {
        mPopularVO = (ParentLearningVO) data;
        this.tvPopularTitle.setText(mPopularVO.getTitle());

        Glide.with(ivMostPopular.getContext())
                .load(mPopularVO.getImageUrl())
                .into(ivMostPopular);
    }

    @Override
    public void onClick(View view) {

    }

    @OnClick(R.id.cv_do_and_donot_popular)
    public void onTapMostPopularItem(View view){
        //Toast.makeText(view.getContext(),"detail",Toast.LENGTH_SHORT).show();
        mParentDelegate.onTapMostPopularItem(mPopularVO);
    }
}
