package com.padcmyanmar.fun3.pcl.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.padcmyanmar.fun3.pcl.R;
import com.padcmyanmar.fun3.pcl.adapters.ChildCategoryAdapter;
import com.padcmyanmar.fun3.pcl.data.model.ChildLearningModel;
import com.padcmyanmar.fun3.pcl.fragments.AlphaGameFragment;
import com.padcmyanmar.fun3.pcl.fragments.AnimalsGameFragment;
import com.padcmyanmar.fun3.pcl.fragments.ColorFragment;
import com.padcmyanmar.fun3.pcl.fragments.ShapeCircleFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by htoo on 2/19/2018.
 */

public class ChildCategoryActivity extends BaseActivity {

    @BindView(R.id.tl_child_category)
    TabLayout tlChildCategory;

    @BindView(R.id.vp_child_category)
    ViewPager vpChildCategory;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private ChildCategoryAdapter mChildCategoryAdapter;

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, ChildCategoryActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_child_category);

        ButterKnife.bind(this, this);

        ChildLearningModel.getsObjectInstance().loadLearning();

        addToolbar();

        mChildCategoryAdapter = new ChildCategoryAdapter(getSupportFragmentManager());
        vpChildCategory.setAdapter(mChildCategoryAdapter);

        mChildCategoryAdapter.addTab("Animals", new AnimalsGameFragment());
        mChildCategoryAdapter.addTab("Colors", ColorFragment.newInstance());
        mChildCategoryAdapter.addTab("Shapes", new ShapeCircleFragment());
        mChildCategoryAdapter.addTab("Alpha", new AlphaGameFragment());

        tlChildCategory.setupWithViewPager(vpChildCategory);

    }

    private void addToolbar() {
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.child_category_title);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_24dp);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }
}
