package com.padcmyanmar.fun3.pcl.viewholders;

import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.padcmyanmar.fun3.pcl.R;
import com.padcmyanmar.fun3.pcl.adapters.NutritionForBabiesAdapter;
import com.padcmyanmar.fun3.pcl.data.model.LoginUserModel;
import com.padcmyanmar.fun3.pcl.data.model.ParentLearningModel;
import com.padcmyanmar.fun3.pcl.data.vo.FavoriteUserVO;
import com.padcmyanmar.fun3.pcl.data.vo.LoginVO;
import com.padcmyanmar.fun3.pcl.data.vo.ParentLearningVO;
import com.padcmyanmar.fun3.pcl.delegates.ParentLearningActionDelegates;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by User on 2/20/2018.
 */

public class NutritionForBabiesViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.iv_nutrition_image)
    ImageView ivNutrition;

    @BindView(R.id.tv_nutrition_title)
    TextView tvNutritionTitle;

    @BindView(R.id.tv_nutrition_brief)
    TextView tvNutritionBrief;

    @BindView(R.id.tv_no_of_favourites)
    TextView tvNoOfFavourites;

    @BindView(R.id.tv_no_of_shares)
    TextView tvNoOfShares;

    @BindView(R.id.btn_favourite)
    TextView btnFavorite;

    private ParentLearningActionDelegates mParentLearningActionDelegates;
    private ParentLearningVO mParentLearning;

    public NutritionForBabiesViewHolder(View itemView, ParentLearningActionDelegates parentLearningActionDelegates) {
        super(itemView);
        ButterKnife.bind(this,itemView);
        mParentLearningActionDelegates = parentLearningActionDelegates;
    }

    @OnClick(R.id.tv_nutrition_brief)
    public void onNutritionBriefTap(View view)
    {
       // Toast.makeText(view.getContext(),"Nutrition Item Clicked", Toast.LENGTH_LONG).show();
        mParentLearningActionDelegates.onTapInfoBrief(mParentLearning);
    }

    public void setParentLearningData(ParentLearningVO parentLearning){
        mParentLearning = parentLearning;

            Glide.with(ivNutrition.getContext())
                    .load(parentLearning.getImageUrl())
                    .into(ivNutrition);

            tvNutritionTitle.setText(parentLearning.getTitle());
            tvNutritionBrief.setText(parentLearning.getDescription());
            tvNoOfFavourites.setText(tvNoOfFavourites.getContext().getResources().getString(R.string.format_favourite_users,
                    parentLearning.getFavUserList()));
            tvNoOfShares.setText(tvNoOfShares.getContext().getResources().getString(R.string.format_shares_users,
                    parentLearning.getShareUserList()));

        /**
         * to make a collection of publication with ImageUrl , text
         */
    }

    @OnClick(R.id.btn_favourite)
    public void onTapFavoriteNutrition(View view){
        LoginVO loginUser = LoginUserModel.getsObjInstance(view.getContext()).getLoginUser();
        FavoriteUserVO loginFavUser = new FavoriteUserVO();

        if (loginUser != null){
            loginFavUser.setUserId(loginUser.getUserId());
            loginFavUser.setUserName(loginUser.getName());

            List<FavoriteUserVO> favoriteUserList;
            favoriteUserList = ParentLearningModel.getsObjectInstance().getmParentLearningList().get(mParentLearning.getId()-1).getFavoriteUserList();
            for (FavoriteUserVO favUser : favoriteUserList){
                if (favUser.getUserId() == loginFavUser.getUserId()){
                    Toast.makeText(view.getContext(), R.string.already_exit, Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    ParentLearningModel.getsObjectInstance().getmParentLearningList().get(mParentLearning.getId()-1).getFavoriteUserList().add(loginFavUser);
                    btnFavorite.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_favorite_24dp,0,0,0);
                    return;
                }
            }
        } else {
            Snackbar.make(btnFavorite,"Please Login First", Snackbar.LENGTH_SHORT).show();
        }
    }
}
