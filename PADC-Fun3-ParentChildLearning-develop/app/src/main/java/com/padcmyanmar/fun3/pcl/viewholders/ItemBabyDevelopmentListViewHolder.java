package com.padcmyanmar.fun3.pcl.viewholders;

import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;

import com.padcmyanmar.fun3.pcl.R;
import com.padcmyanmar.fun3.pcl.data.vo.BabyDevelopmentVO;
import com.padcmyanmar.fun3.pcl.data.vo.OtherSuggestionVO;
import com.padcmyanmar.fun3.pcl.delegates.BabyDevelopmentDelegate;

import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by htoo on 2/20/2018.
 */

public class ItemBabyDevelopmentListViewHolder extends BaseViewHolder {

    @BindView(R.id.tv_date)
    TextView tvDate;
    @BindView(R.id.tv_actual_weight)
    TextView tvActualWeight;
    @BindView(R.id.tv_actual_height)
    TextView tvActualHeight;
    @BindView(R.id.tv_suggestion_weight)
    TextView tvSuggestionWeight;
    @BindView(R.id.tv_suggestion_height)
    TextView tvSuggestionHeight;
    @BindView(R.id.tv_suggestion_text)
    TextView tvSuggestionText;
    @BindView(R.id.tv_suggestion_text2)
    TextView tvSuggestionText2;

    private BabyDevelopmentDelegate mBabyDevelopmentDelegate;
    private BabyDevelopmentVO mBabyDevelopment;

    public ItemBabyDevelopmentListViewHolder(View itemView, BabyDevelopmentDelegate babyDevelopmentDelegate) {
        super(itemView);
        ButterKnife.bind(this,itemView);

        mBabyDevelopmentDelegate = babyDevelopmentDelegate;
    }

    @Override
    public void setData(Object data) {
        mBabyDevelopment = (BabyDevelopmentVO) data;

        tvDate.setText(mBabyDevelopment.getDate());
        tvActualWeight.setText(String.valueOf(mBabyDevelopment.getActualWeight()));
        tvActualHeight.setText(String.valueOf(mBabyDevelopment.getActualHeight()));
        tvSuggestionWeight.setText(String.valueOf(mBabyDevelopment.getSuggestionWeight()));
        tvSuggestionHeight.setText(String.valueOf(mBabyDevelopment.getSuggestionHeight()));
        tvSuggestionText.setText(mBabyDevelopment.getOtherSuggestion().get(0).getSuggestion());
        tvSuggestionText2.setText(mBabyDevelopment.getOtherSuggestion().get(1).getSuggestion());

    }

    @Override
    public void onClick(View view) {

    }

    @OnClick(R.id.cv_baby_development_item)
    public void onTapBabyDevelopmentItem(View view){
        mBabyDevelopmentDelegate.onTapBabyDevelopmentItem(mBabyDevelopment);
    }
}
