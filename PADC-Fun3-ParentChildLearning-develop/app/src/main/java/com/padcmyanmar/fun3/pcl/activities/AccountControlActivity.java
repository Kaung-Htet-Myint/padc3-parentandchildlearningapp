package com.padcmyanmar.fun3.pcl.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toolbar;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.padcmyanmar.fun3.pcl.PCLApp;
import com.padcmyanmar.fun3.pcl.R;
import com.padcmyanmar.fun3.pcl.data.vo.LoginVO;
import com.padcmyanmar.fun3.pcl.delegates.LoginScreenDelegates;
import com.padcmyanmar.fun3.pcl.fragments.LoginFragment;
import com.padcmyanmar.fun3.pcl.fragments.RegisterFragment;
import com.padcmyanmar.fun3.pcl.viewpods.AccountControlViewPod;
import com.padcmyanmar.fun3.pcl.viewpods.LoginUserViewPod;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by htoo on 2/19/2018.
 */

public class AccountControlActivity extends BaseActivity implements LoginScreenDelegates, GoogleApiClient.OnConnectionFailedListener {

    private static final String IE_SCREEN_TYPE = "IE_SCREEN_TYPE";
    private static final int SCREEN_TYPE_LOGIN = 1;
    private static final int SCREEN_TYPE_REGISTER = 2;

    private GoogleApiClient mGoogleApiClient;
    private LoginUserViewPod vpLoginUser;
    private LoginVO loginVO;
    /*@BindView(R.id.toolbar)
    android.support.v7.widget.Toolbar toolbar;*/

    public static Intent newIntentLogin(Context context){
        Intent intent = new Intent(context, AccountControlActivity.class);
        intent.putExtra(IE_SCREEN_TYPE, SCREEN_TYPE_LOGIN);
        return intent;
    }

    public static Intent newIntentRegister(Context context){
        Intent intent = new Intent(context, AccountControlActivity.class);
        intent.putExtra(IE_SCREEN_TYPE, SCREEN_TYPE_REGISTER);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_control);

        ButterKnife.bind(this,this);

        /*setSupportActionBar(toolbar);

        if(getSupportActionBar() != null)
        {
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_24dp);
            getSupportActionBar().setTitle("Login Here");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
*/
        int screenType = getIntent().getIntExtra(IE_SCREEN_TYPE, -1);
        if(screenType == SCREEN_TYPE_LOGIN){
            // to call login fragment
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fl_container, new LoginFragment())
                    .commit();
        } else if (screenType == SCREEN_TYPE_REGISTER){
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fl_container, new RegisterFragment())
                    .commit();
        }

        setupGoogleApiClient();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1000) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                // Google Sign-In was successful, authenticate with Firebase
                GoogleSignInAccount account = result.getSignInAccount();
                Toast.makeText(getApplicationContext(), "Google Sign-In success : "
                        + account.getDisplayName(), Toast.LENGTH_SHORT).show();
            } else {
                // Google Sign-In failed
                Log.e(PCLApp.LOG_TAG, "Google Sign-In failed.");
                Toast.makeText(getApplicationContext(), "Google Sign-In failed.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onTapToLogin() {
        //onTapLogin in main activity
    }

    @Override
    public void onTapTORegister() {
        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                .replace(R.id.fl_container, new RegisterFragment())
                .addToBackStack("ToRegister")
                .commit();
    }

    @Override
    public void onTapLoginWithGoogle() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent,1000);
    }

    private void setupGoogleApiClient(){
        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken("326628500404-80kq0teijmbjbd4baie9o4qs1inustbg.apps.googleusercontent.com")
                .requestEmail()
                .build();

        mGoogleApiClient = new  GoogleApiClient.Builder(getApplicationContext())
                .enableAutoManage(this,this)
                .addApi(Auth.GOOGLE_SIGN_IN_API,googleSignInOptions)
                .build();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
