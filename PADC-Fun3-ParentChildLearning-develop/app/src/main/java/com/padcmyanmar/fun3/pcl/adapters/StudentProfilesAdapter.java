package com.padcmyanmar.fun3.pcl.adapters;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dell on 2/18/2018.
 */

public class StudentProfilesAdapter extends FragmentStatePagerAdapter {

    private List<Fragment> mFragments;
    private List<String> titles;

    public StudentProfilesAdapter(FragmentManager fm) {
        super(fm);
        mFragments = new ArrayList<>();
        titles = new ArrayList<>();
    }

    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position);
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {  //return title
        return titles.get(position);
    }

    public void addTab(String tabTitle, Fragment fragment) {
        titles.add(tabTitle);
        mFragments.add(fragment);
        //notifyDataSetChanged();
    }

}
