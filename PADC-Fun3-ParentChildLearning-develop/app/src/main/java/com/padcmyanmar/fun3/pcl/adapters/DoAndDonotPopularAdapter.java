package com.padcmyanmar.fun3.pcl.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.padcmyanmar.fun3.pcl.R;
import com.padcmyanmar.fun3.pcl.data.vo.ParentLearningVO;
import com.padcmyanmar.fun3.pcl.delegates.ParentDelegate;
import com.padcmyanmar.fun3.pcl.viewholders.ItemDoAndDonotPopularViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by einandartun on 2/25/18.
 */

public class DoAndDonotPopularAdapter extends RecyclerView.Adapter<ItemDoAndDonotPopularViewHolder> {

    private ParentDelegate mParentDelegate;
    private List<ParentLearningVO> mPopularParentLearningList;

    public DoAndDonotPopularAdapter(ParentDelegate parentDelegate) {
        mParentDelegate = parentDelegate;
        mPopularParentLearningList = new ArrayList<>();
    }

    @Override
    public ItemDoAndDonotPopularViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View doAndDonotView = inflater.inflate(R.layout.item_do_and_donot_most_popular,parent,false);
        ItemDoAndDonotPopularViewHolder itemPopularViewHolder = new ItemDoAndDonotPopularViewHolder(doAndDonotView, mParentDelegate);
        return itemPopularViewHolder;
    }

    @Override
    public void onBindViewHolder(ItemDoAndDonotPopularViewHolder holder, int position) {
        holder.setData(mPopularParentLearningList.get(position));
    }

    @Override
    public int getItemCount() {
        return mPopularParentLearningList.size();
    }

    public void setPopularParentData(List<ParentLearningVO> learningList){
        mPopularParentLearningList = learningList;
        notifyDataSetChanged();

    }
}
