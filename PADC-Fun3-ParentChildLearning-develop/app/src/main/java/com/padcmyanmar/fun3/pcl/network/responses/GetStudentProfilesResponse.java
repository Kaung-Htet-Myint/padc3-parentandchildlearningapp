package com.padcmyanmar.fun3.pcl.network.responses;

import com.google.gson.annotations.SerializedName;
import com.padcmyanmar.fun3.pcl.data.vo.StudentProfileVO;

import java.util.List;

public class GetStudentProfilesResponse {

    @SerializedName("code")
    private int code;

    @SerializedName("message")
    private String message;

    @SerializedName("apiVersion")
    private String apiVersion;

    @SerializedName("studentProfiles")
    private List<StudentProfileVO> studentProfiles;

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String getApiVersion() {
        return apiVersion;
    }

    public List<StudentProfileVO> getStudentProfiles() {
        return studentProfiles;
    }
}
