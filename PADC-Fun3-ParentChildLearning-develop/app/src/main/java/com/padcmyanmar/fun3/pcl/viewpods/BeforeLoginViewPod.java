package com.padcmyanmar.fun3.pcl.viewpods;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

import com.padcmyanmar.fun3.pcl.R;
import com.padcmyanmar.fun3.pcl.delegates.BeforeLoginDelegate;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by htoo on 2/18/2018.
 */

public class BeforeLoginViewPod extends RelativeLayout {

    private BeforeLoginDelegate mBeforeLoginDelegate;

    public BeforeLoginViewPod(Context context) {
        super(context);
    }

    public BeforeLoginViewPod(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BeforeLoginViewPod(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this,this);
    }

    public void setDelegate(BeforeLoginDelegate beforeLoginDelegate){
        mBeforeLoginDelegate = beforeLoginDelegate;
    }

    @OnClick(R.id.btn_to_login)
    public void onTapLogin(View view){
        mBeforeLoginDelegate.onTapToLogin();
    }

    @OnClick(R.id.btn_to_register)
    public void onTapRegister(View view){
        mBeforeLoginDelegate.onTapRegister();
    }
}
