package com.padcmyanmar.fun3.pcl.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.padcmyanmar.fun3.pcl.R;

import butterknife.ButterKnife;

/**
 * Created by htoo on 2/23/2018.
 */

public class FavoriteDetailsCategoryActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite_detail);
        ButterKnife.bind(this,this);
    }
}
