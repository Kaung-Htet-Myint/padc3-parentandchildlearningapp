package com.padcmyanmar.fun3.pcl.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.padcmyanmar.fun3.pcl.R;
import com.padcmyanmar.fun3.pcl.data.vo.ParentLearningVO;
import com.padcmyanmar.fun3.pcl.delegates.ParentDelegate;
import com.padcmyanmar.fun3.pcl.viewholders.ItemDoAndDonotPopularViewHolder;
import com.padcmyanmar.fun3.pcl.viewholders.ItemDoAndDonotViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by einandartun on 2/19/18.
 */

public class DoAndDonotAdapter extends RecyclerView.Adapter<ItemDoAndDonotViewHolder> {

    private ParentDelegate mParentDelegate;
    private List<ParentLearningVO> mParentLearningList;

    public DoAndDonotAdapter(ParentDelegate parentDelegate){
        mParentDelegate = parentDelegate;
        mParentLearningList = new ArrayList<>();
    }

    @Override
    public ItemDoAndDonotViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View doAndDonotView = inflater.inflate(R.layout.item_do_and_donot,parent,false);
        ItemDoAndDonotViewHolder itemDoAndDonotViewHolder = new ItemDoAndDonotViewHolder(doAndDonotView,mParentDelegate);
        return itemDoAndDonotViewHolder;
    }

    @Override
    public void onBindViewHolder(ItemDoAndDonotViewHolder holder, int position) {
        holder.setData(mParentLearningList.get(position));
    }

    @Override
    public int getItemCount() {
        return mParentLearningList.size();
    }

    public void setParentData(List<ParentLearningVO> learningList){
        mParentLearningList = learningList;
        notifyDataSetChanged();

    }
}
