package com.padcmyanmar.fun3.pcl.viewholders;

import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.padcmyanmar.fun3.pcl.R;
import com.padcmyanmar.fun3.pcl.data.model.LoginUserModel;
import com.padcmyanmar.fun3.pcl.data.model.ParentLearningModel;
import com.padcmyanmar.fun3.pcl.data.vo.FavoriteUserVO;
import com.padcmyanmar.fun3.pcl.data.vo.LoginVO;
import com.padcmyanmar.fun3.pcl.data.vo.ParentLearningVO;
import com.padcmyanmar.fun3.pcl.delegates.LearningTipsDelegate;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by daewichan on 2/19/18.
 */

public class ItemLearningTipsHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.iv_learning_educate)
    ImageView ivTipsImage;

    @BindView(R.id.tv_learning_sub_title)
    TextView tvTipsTitle;

    @BindView(R.id.tv_learning_brief)
    TextView tvTipsBrief;

    @BindView(R.id.btn_favourite)
    TextView tvFavourite;

    @BindView(R.id.tv_share)
    TextView tvShare;


    private LearningTipsDelegate learningTipsDelegate;

    private int curIndex = 0;

//    private List<ParentLearningVO> parentLearningVO;

    private ParentLearningVO parentLearningVO;

    public ItemLearningTipsHolder(View itemView, LearningTipsDelegate learningTipsDelegate) {
        super(itemView);
        this.learningTipsDelegate = learningTipsDelegate;
        ButterKnife.bind(this, itemView);
    }

    @OnClick(R.id.cv_learning_educate)
    public void onLearningTipsItemTap(View view) {
        learningTipsDelegate.onTapItem(parentLearningVO);
        //learningTipsDelegate.onTapItem();
    }

    public void showLearningTip(ParentLearningVO parentLearningVO) {

        this.parentLearningVO = parentLearningVO;

        if (parentLearningVO.getType() == 2) {

            Glide.with(ivTipsImage.getContext())
                    .load(parentLearningVO.getImageUrl())
                    .into(ivTipsImage);

            tvTipsTitle.setText(parentLearningVO.getTitle());
            tvTipsBrief.setText(parentLearningVO.getDescription());

            tvFavourite.setText(parentLearningVO.getFavUserList()+" Favourites");
            tvShare.setText(parentLearningVO.getShareUserList()+" Shares");
        }

//        btnFavorite.setText(parentLearningVO.getFavUserList());

    }

    @OnClick(R.id.btn_favourite)
    public void onTapFavoriteList(View view){
        LoginVO loginUser = LoginUserModel.getsObjInstance(view.getContext()).getLoginUser();
        FavoriteUserVO loginFavUser = new FavoriteUserVO();

        if (loginUser != null){
            loginFavUser.setUserId(loginUser.getUserId());
            loginFavUser.setUserName(loginUser.getName());

            List<FavoriteUserVO> favoriteUserList;
            favoriteUserList = ParentLearningModel.getsObjectInstance().getmParentLearningList().get(parentLearningVO.getId()-1).getFavoriteUserList();
            for (FavoriteUserVO favUser : favoriteUserList){
                if (favUser.getUserId() == loginFavUser.getUserId()){
                    Toast.makeText(view.getContext(), R.string.already_exit, Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    ParentLearningModel.getsObjectInstance().getmParentLearningList().get(parentLearningVO.getId()-1).getFavoriteUserList().add(loginFavUser);
                    tvFavourite.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_favorite_24dp,0,0,0);
                    return;
                }
            }
        }else {
            Snackbar.make(tvFavourite,"Please Login First", Snackbar.LENGTH_SHORT).show();
        }
    }


}
