package com.padcmyanmar.fun3.pcl.event;

import com.padcmyanmar.fun3.pcl.data.vo.StudentProfileVO;

import java.util.ArrayList;
import java.util.List;

public class StudentProfilesLoadedEvent {

    private List<StudentProfileVO> studentProfiles;

    public StudentProfilesLoadedEvent(List<StudentProfileVO> studentProfiles) {
        this.studentProfiles = studentProfiles;
    }

    public List<StudentProfileVO> getStudentProfiles() {
        if (studentProfiles == null)
            return new ArrayList<>();

        return studentProfiles;
    }
}
