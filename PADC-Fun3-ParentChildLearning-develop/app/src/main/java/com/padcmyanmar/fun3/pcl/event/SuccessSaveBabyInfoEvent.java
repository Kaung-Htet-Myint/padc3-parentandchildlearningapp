package com.padcmyanmar.fun3.pcl.event;

import com.padcmyanmar.fun3.pcl.data.vo.BabyInfoVO;

/**
 * Created by htoo on 4/1/2018.
 */

public class SuccessSaveBabyInfoEvent {
    private BabyInfoVO mBabyInfo;

    public SuccessSaveBabyInfoEvent(BabyInfoVO mBabyInfo) {
        this.mBabyInfo = mBabyInfo;
    }

    public BabyInfoVO getmBabyInfo() {
        return mBabyInfo;
    }
}
