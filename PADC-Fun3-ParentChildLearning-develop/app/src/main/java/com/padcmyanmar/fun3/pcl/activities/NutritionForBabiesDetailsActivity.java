package com.padcmyanmar.fun3.pcl.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.padcmyanmar.fun3.pcl.R;
import com.padcmyanmar.fun3.pcl.adapters.ImageInNutritionForBabiesDetailsAdapter;
import com.padcmyanmar.fun3.pcl.data.model.LoginUserModel;
import com.padcmyanmar.fun3.pcl.data.model.ParentLearningModel;
import com.padcmyanmar.fun3.pcl.data.vo.FavoriteUserVO;
import com.padcmyanmar.fun3.pcl.data.vo.LoginVO;
import com.padcmyanmar.fun3.pcl.data.vo.ParentLearningVO;

import org.w3c.dom.Text;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by User on 2/21/2018.
 */

public class NutritionForBabiesDetailsActivity extends BaseActivity {

    private ParentLearningVO parentLearningVO;

    //private ImageInNutritionForBabiesDetailsAdapter mImageInNutritionForBabiesDetailsAdapter;

    @BindView(R.id.nutrition_toolbar)
    Toolbar nutritionToolbar;

    /*@BindView(R.id.vp_nutrition_details_images)
    ViewPager vpNutritionDetailsImages;*/

    @BindView(R.id.iv_nutrition_detail_image)
    ImageView ivNutritionDetailImage;

    @BindView(R.id.tv_nutrition_title_in_details)
    TextView tvNutritionTitleInDeatails;

    @BindView(R.id.tv_news_details)
    TextView tvNewsDetails;

    @BindView(R.id.fab_favorite_news)
    FloatingActionButton favFavorite;

    //private List<String> imgUrlList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nutrition_for_baby_details);
        ButterKnife.bind(this,this);

        setSupportActionBar(nutritionToolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_24dp);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        int id=1;
        int parentLearningId = getIntent().getIntExtra("pLearning_id",id);
        ParentLearningVO pLearning = ParentLearningModel.getsObjectInstance().getLearningById(parentLearningId);


        bindData(pLearning);
       //mImageInNutritionForBabiesDetailsAdapter = new ImageInNutritionForBabiesDetailsAdapter(imgUrlList);
       //vpNutritionDetailsImages.setAdapter(mImageInNutritionForBabiesDetailsAdapter);
    }

    private void bindData(ParentLearningVO pLearning){
        parentLearningVO =  pLearning;
        Glide.with(ivNutritionDetailImage.getContext())
                .load(pLearning.getImageUrl())
                .into(ivNutritionDetailImage);

        tvNutritionTitleInDeatails.setText(pLearning.getTitle());
        tvNewsDetails.setText(pLearning.getDescription());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
        {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.fab_favorite_news)
    public void onTapFavorite(View view){
        LoginVO loginUser = LoginUserModel.getsObjInstance(view.getContext()).getLoginUser();
        FavoriteUserVO favoriteUser = new FavoriteUserVO();

        if (loginUser !=null){
            favoriteUser.setUserId(loginUser.getUserId());
            favoriteUser.setUserName(loginUser.getName());

            List<FavoriteUserVO> favoriteUserList;
            favoriteUserList = ParentLearningModel.getsObjectInstance().getmParentLearningList().get(parentLearningVO.getId()-1).getFavoriteUserList();
            for (FavoriteUserVO favUser : favoriteUserList){
                if (favUser.getUserId() == favoriteUser.getUserId()){
                    Toast.makeText(view.getContext(), getResources().getString(R.string.already_exit), Toast.LENGTH_SHORT).show();
                    break;
                } else {
                    ParentLearningModel.getsObjectInstance().getmParentLearningList().get(parentLearningVO.getId()-1).getFavoriteUserList().add(favoriteUser);
                    favFavorite.setImageResource(R.drawable.ic_favorite_24dp);
                    break;
                }
            }
        } else {
            Snackbar.make(favFavorite,"Please Login First", Snackbar.LENGTH_SHORT).show();
        }

    }
}
