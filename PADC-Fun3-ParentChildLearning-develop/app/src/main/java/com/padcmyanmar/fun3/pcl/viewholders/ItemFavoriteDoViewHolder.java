package com.padcmyanmar.fun3.pcl.viewholders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.padcmyanmar.fun3.pcl.R;
import com.padcmyanmar.fun3.pcl.data.vo.ParentLearningVO;
import com.padcmyanmar.fun3.pcl.delegates.FavoriteDelegate;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by htoo on 2/23/2018.
 */

public class ItemFavoriteDoViewHolder extends BaseViewHolder {

    private FavoriteDelegate mFavoriteDelegate;
    private ParentLearningVO mFavoriteDoAndDonot;

    @BindView(R.id.tv_title)
    TextView tvTitle;

    @BindView(R.id.iv_title_image)
    ImageView ivTitleImage;

    public ItemFavoriteDoViewHolder(View itemView, FavoriteDelegate favoriteDelegate) {
        super(itemView);
        ButterKnife.bind(this,itemView);

        mFavoriteDelegate = favoriteDelegate;
    }

    @Override
    public void setData(Object data) {
        mFavoriteDoAndDonot = (ParentLearningVO) data;
        this.tvTitle.setText(mFavoriteDoAndDonot.getTitle());
        Glide.with(ivTitleImage.getContext())
                .load(mFavoriteDoAndDonot.getImageUrl())
                .into(ivTitleImage);
    }

    @Override
    public void onClick(View view) {

    }

    @OnClick(R.id.cv_favorite_item)
    public void onTapFavoriteDoAndDonotDetails(View view){
//        Toast.makeText(view.getContext(), "Tapped", Toast.LENGTH_SHORT).show();
        mFavoriteDelegate.onTapFavoriteDoAndDonotDetails(mFavoriteDoAndDonot);
    }

    @OnClick(R.id.iv_share)
    public void onTapShare(View view){
        mFavoriteDelegate.onTapShareButton();
    }
}
