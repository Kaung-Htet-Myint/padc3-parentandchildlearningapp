package com.padcmyanmar.fun3.pcl.data.vo;

import com.google.gson.annotations.SerializedName;

import org.greenrobot.eventbus.Subscribe;

import java.util.List;

/**
 * Created by daewichan on 3/4/18.
 */

public class ParentLearningVO {
    @SerializedName("id")
    private int id;

    @SerializedName("image_url")
    private String imageUrl;

    @SerializedName("title")
    private String title;

    @SerializedName("desc")
    private String desc;

    @SerializedName("fav_user_list")
    private int favUserList;

    @SerializedName("share_user_list")
    private int shareUserList;

    @SerializedName("type")
    private int type;

    @SerializedName("is_most_popular")
    private int isMostPopular;

    @SerializedName("is_favorite")
    private int isFavorite;

    @SerializedName("favorite_user")
    private List<FavoriteUserVO> favoriteUserList;

    public int getIsFavorite() {
        return isFavorite;
    }

    public int getId() {
        return id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return desc;
    }

    public int getShareUserList() {
        return shareUserList;
    }

    public int getType() {

        return type;
    }

    public int getIsMostPopular() {
        return isMostPopular;
    }

    public int getFavUserList() {

        return favUserList;
    }

    public List<FavoriteUserVO> getFavoriteUserList() {
        return favoriteUserList;
    }
}
