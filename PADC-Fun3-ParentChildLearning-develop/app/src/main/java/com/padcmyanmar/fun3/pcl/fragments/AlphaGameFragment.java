package com.padcmyanmar.fun3.pcl.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.padcmyanmar.fun3.pcl.PCLApp;
import com.padcmyanmar.fun3.pcl.R;
import com.padcmyanmar.fun3.pcl.data.model.ChildLearningModel;
import com.padcmyanmar.fun3.pcl.data.vo.ChildLearningVO;
import com.padcmyanmar.fun3.pcl.event.LoadedChildLearningEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by htoo on 2/20/2018.
 */

public class AlphaGameFragment extends BaseFragment implements TextToSpeech.OnInitListener {

    @BindView(R.id.iv_alpha)
    ImageView ivAlpha;
    @BindView(R.id.btn_previous)
    ImageView btnPrevious;
    @BindView(R.id.btn_play)
    ImageView btnPlay;
    @BindView(R.id.btn_next)
    ImageView btnNext;
    @BindView(R.id.tv_alpha)
    TextView tvAlpha;
    @BindView(R.id.rl_fragment_alpha)
    RelativeLayout rlFragmentAlpha;

    private TextToSpeech textToSpeech;
    private static AlphaGameFragment alphaGameFragment;

    private List<ChildLearningVO> mChildLearningList;

    ArrayList<String> alphaImage;
    ArrayList<String> alphas;

    private ProgressDialog mProgressDialog;

    private int index;

    private static AlphaGameFragment newInstance() {
        if (alphaGameFragment == null) {
            alphaGameFragment = new AlphaGameFragment();
        }
        return alphaGameFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_alpha_game, container, false);
        ButterKnife.bind(this, view);

        rlFragmentAlpha.setVisibility(View.INVISIBLE);

        textToSpeech = new TextToSpeech(getActivity(), this);

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Please wait while data is loading");
        mProgressDialog.show();

        index = 0;

        hideBackArrow();

        mChildLearningList = ChildLearningModel.getsObjectInstance().getChildLearningList();
        if (mChildLearningList.size() != 0){
            mProgressDialog.dismiss();

            alphaImage = new ArrayList<>();
            alphas = new ArrayList<>();

            try {
                if(!mChildLearningList.isEmpty()){
                    for (int i=0; i<mChildLearningList.size(); i++){
                        rlFragmentAlpha.setVisibility(View.VISIBLE);
                        hideBackArrow();

                        if(mChildLearningList.get(i).getType() == 4) {
                            alphaImage.add(mChildLearningList.get(i).getImageUrl());
                            alphas.add(mChildLearningList.get(i).getText());
                        }
                    }
                }
            } catch (NullPointerException ex) {
                Snackbar.make(rlFragmentAlpha, "Please check your internet connection", Snackbar.LENGTH_INDEFINITE).show();
            }
        }else {
            Snackbar.make(rlFragmentAlpha,"Please check your internet connection",Snackbar.LENGTH_INDEFINITE).show();
        }


        return view;
    }

    private void showBackArrow() {
        btnPrevious.setVisibility(View.VISIBLE);
    }

    private void hideBackArrow() {
        btnPrevious.setVisibility(View.INVISIBLE);
    }

    private void showForwardArrow() {
        btnNext.setVisibility(View.VISIBLE);
    }

    private void hideForwardArrow() {
        btnNext.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {

            int result = textToSpeech.setLanguage(Locale.US);

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e(PCLApp.LOG_TAG, "This Language is not supported");
            }

        } else {
            Log.e(PCLApp.LOG_TAG, "Initilization Failed!");
        }
    }

    @OnClick(R.id.iv_alpha)
    public void onClickImage(View view){
        speakOut();
    }

    @OnClick(R.id.btn_next)
    public void onClickNext(View view){
        if(index<alphaImage.size()){
            Glide.with(ivAlpha.getContext())
                    .load(alphaImage.get(index))
                    .into(ivAlpha);
            tvAlpha.setText(alphas.get(index));

            showBackArrow();

            index++;
        }else{
            hideForwardArrow();
        }
    }

    @OnClick(R.id.btn_previous)
    public void onClickPrevious(View view){
        if(index != 0){
            index--;

            Glide.with(ivAlpha.getContext())
                    .load(alphaImage.get(index))
                    .into(ivAlpha);
            tvAlpha.setText(alphas.get(index));

//            speakOut();
            showForwardArrow();
        }else{
            hideBackArrow();
        }
    }

    private void speakOut() {
        String text = tvAlpha.getText().toString();
        textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAlphaGameLoaded(LoadedChildLearningEvent event){
//        Log.d(PCLApp.LOG_TAG, "onAlphaGameLoaded : " + event.getChildLearningList().size());
        mProgressDialog.dismiss();

        alphaImage = new ArrayList<>();
        alphas = new ArrayList<>();

        try {
            if(!event.getChildLearningList().isEmpty()){
                for (int i=0; i<event.getChildLearningList().size(); i++){
                    rlFragmentAlpha.setVisibility(View.VISIBLE);
                    hideBackArrow();

                    if(event.getChildLearningList().get(i).getType() == 4) {
                        alphaImage.add(event.getChildLearningList().get(i).getImageUrl());
                        alphas.add(event.getChildLearningList().get(i).getText());
                    }
                }
            }
        } catch (NullPointerException ex) {
            Snackbar.make(rlFragmentAlpha, "Please check your internet connection", Snackbar.LENGTH_INDEFINITE).show();
        }

    }
}
