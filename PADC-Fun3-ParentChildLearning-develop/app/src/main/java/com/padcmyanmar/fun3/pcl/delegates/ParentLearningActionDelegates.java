package com.padcmyanmar.fun3.pcl.delegates;

import com.padcmyanmar.fun3.pcl.data.vo.ParentLearningVO;

/**
 * Created by User on 2/25/2018.
 */

public interface ParentLearningActionDelegates {
    void onTapNutritionItem();
    void onTapInfoBrief(ParentLearningVO parentLearningVO);
    void onTapFavButton();
    void onTapCommentButton();
    void onTapShareButton();
    void onTapSaveButton();
}
