package com.padcmyanmar.fun3.pcl.activities;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ShareCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.SearchView;
import android.widget.TextView;

import com.padcmyanmar.fun3.pcl.R;
import com.padcmyanmar.fun3.pcl.adapters.ItemFavoriteListDoAdapter;
import com.padcmyanmar.fun3.pcl.adapters.ItemFavoriteListLearningAdapter;
import com.padcmyanmar.fun3.pcl.adapters.ItemFavoriteListNutritionAdapter;
import com.padcmyanmar.fun3.pcl.data.model.LoginUserModel;
import com.padcmyanmar.fun3.pcl.data.model.ParentLearningModel;
import com.padcmyanmar.fun3.pcl.data.vo.FavoriteUserVO;
import com.padcmyanmar.fun3.pcl.data.vo.LoginVO;
import com.padcmyanmar.fun3.pcl.data.vo.ParentLearningVO;
import com.padcmyanmar.fun3.pcl.delegates.FavoriteDelegate;
import com.padcmyanmar.fun3.pcl.event.LoadedParentLearningEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by htoo on 2/22/2018.
 */

public class FavoriteListCategoryActivity extends BaseActivity implements FavoriteDelegate,SearchView.OnQueryTextListener{

    @BindView(R.id.rv_favorites_do)
    RecyclerView rvFavorite;
    @BindView(R.id.rv_favorites_learngin_tips)
    RecyclerView rvFavoriteLearning;
    @BindView(R.id.rv_favorites_nutrition)
    RecyclerView rvFavoriteNutrition;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_do_donnot_label)
    TextView tvDoAndDonot;

    @BindView(R.id.tv_learning_tips_label)
    TextView tvLearningTips;

    @BindView(R.id.tv_nutrition)
    TextView tvNutrition;

    private ItemFavoriteListDoAdapter mItemFavoriteDoAndDonotAdapter;
    private ItemFavoriteListLearningAdapter mItemFavoriteListLearningAdapter;
    private ItemFavoriteListNutritionAdapter mItemFavoriteListNutritionAdapter;

    private List<ParentLearningVO> mParentLearningList;

    public static Intent newIntent(Context context){
        Intent intent = new Intent(context, FavoriteListCategoryActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite_list_category);
        ButterKnife.bind(this,this);

        mParentLearningList = new ArrayList<>();

        addToolBar();

        mItemFavoriteDoAndDonotAdapter = new ItemFavoriteListDoAdapter(this);
        rvFavorite.setAdapter(mItemFavoriteDoAndDonotAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        rvFavorite.setLayoutManager(linearLayoutManager);

        mItemFavoriteListLearningAdapter = new ItemFavoriteListLearningAdapter(this);
        rvFavoriteLearning.setAdapter(mItemFavoriteListLearningAdapter);
        LinearLayoutManager linearLayoutManagerLearning = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        rvFavoriteLearning.setLayoutManager(linearLayoutManagerLearning);

        mItemFavoriteListNutritionAdapter = new ItemFavoriteListNutritionAdapter(this);
        rvFavoriteNutrition.setAdapter(mItemFavoriteListNutritionAdapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        rvFavoriteNutrition.setLayoutManager(layoutManager);
        hideTextView();

//        ParentLearningModel.getsObjectInstance().loadParentLearning();
        addFavoriteListData();
    }

    private void hideTextView() {
        tvDoAndDonot.setVisibility(View.GONE);
        tvLearningTips.setVisibility(View.GONE);
        tvNutrition.setVisibility(View.GONE);
    }
    private void showTextView() {
        tvDoAndDonot.setVisibility(View.VISIBLE);
        tvLearningTips.setVisibility(View.VISIBLE);
        tvNutrition.setVisibility(View.VISIBLE);
    }

    private void addFavoriteListData() {
        List<ParentLearningVO> favoriteDoAndDonotList = new ArrayList<>();
        List<ParentLearningVO> favoriteLearningList = new ArrayList<>();
        List<ParentLearningVO> favoriteNutritionList = new ArrayList<>();

        LoginVO loginUser = LoginUserModel.getsObjInstance(getApplicationContext()).getLoginUser();
        mParentLearningList = ParentLearningModel.getsObjectInstance().getmParentLearningList();

        if(loginUser != null){
            if (!mParentLearningList.isEmpty()){
                showTextView();
                for (ParentLearningVO parentLearning : mParentLearningList){
                    for (FavoriteUserVO favoriteUser : parentLearning.getFavoriteUserList()){
                        if(parentLearning.getType()==1 && favoriteUser.getUserId() == loginUser.getUserId()){
                            favoriteDoAndDonotList.add(parentLearning);
                        }
                        if (parentLearning.getType() ==2 && favoriteUser.getUserId() == loginUser.getUserId()){
                            favoriteLearningList.add(parentLearning);
                        }
                        if (parentLearning.getType() ==3 && favoriteUser.getUserId() == loginUser.getUserId()){
                            favoriteNutritionList.add(parentLearning);
                        }
                    }
                }

                mItemFavoriteDoAndDonotAdapter.setFavoriteDoAndDonotData(favoriteDoAndDonotList);
                mItemFavoriteListLearningAdapter.setFavoriteLearningData(favoriteLearningList);
                mItemFavoriteListNutritionAdapter.setFavoriteNutritionData(favoriteNutritionList);
            } else {
                ParentLearningModel.getsObjectInstance().loadParentLearning();
            }
        } else {
            Snackbar.make(rvFavorite,getResources().getString(R.string.need_to_login), Snackbar.LENGTH_INDEFINITE).show();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    private void addToolBar() {
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null){
            getSupportActionBar().setTitle(R.string.favorite_menu);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_24dp);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.favorite_search_menu,menu);
        MenuItem item = menu.findItem(R.id.menu_favorite_search);

        final SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(this);
        searchView.setIconifiedByDefault(false);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);
        searchView.setLayoutParams(params);

        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            onBackPressed();
        } else if (item.getItemId() == R.id.menu_favorite_search){
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTapFavoriteDoAndDonotDetails(ParentLearningVO parentLearningVO) {
        Intent intent = new Intent(getApplicationContext(), DoAndDonotDetailActivity.class);
        intent.putExtra("id_do_donot",String.valueOf(parentLearningVO.getId()));
        startActivity(intent);
    }

    @Override
    public void onTapFavoriteLearningDetails(ParentLearningVO parentLearningVO) {
        Intent intent = new Intent(getApplicationContext(), DetailsLearningTipsActivity.class);
        intent.putExtra("id",String.valueOf(parentLearningVO.getId()));
        startActivity(intent);
    }

    @Override
    public void onTapFavoriteNutritionDetails(ParentLearningVO parentLearningNutrition) {
        Intent intent = new Intent(getApplicationContext(), NutritionForBabiesDetailsActivity.class);
        intent.putExtra("pLearning_id",parentLearningNutrition.getId());
        startActivity(intent);
    }

    @Override
    public void onTapShareButton() {
//        Toast.makeText(getApplicationContext(),"click share",Toast.LENGTH_SHORT).show();
        Intent sharedIntent = ShareCompat.IntentBuilder
                .from(this)
                .setType("text/plain")
                .setText(getApplicationContext().getString(R.string.share_article))
                .getIntent();

        if (sharedIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(sharedIntent);
        } else {
            Snackbar.make(rvFavorite, "No App to handle Share Action", Snackbar.LENGTH_INDEFINITE).show();
        }
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        return false;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onFavoriteLoaded(LoadedParentLearningEvent event){
        List<ParentLearningVO> favoriteDoAndDonotList = new ArrayList<>();
        List<ParentLearningVO> favoriteLearningList = new ArrayList<>();
        List<ParentLearningVO> favoriteNutritionList = new ArrayList<>();
        LoginVO loginUser = LoginUserModel.getsObjInstance(getApplicationContext()).getLoginUser();

        if(loginUser != null){
            if (!event.getParentLearningList().isEmpty()){
                for (ParentLearningVO parentLearning : event.getParentLearningList()){
                    for (FavoriteUserVO favoriteUser : parentLearning.getFavoriteUserList()){
                        if(parentLearning.getType()==1 && favoriteUser.getUserId() == loginUser.getUserId()){
                            favoriteDoAndDonotList.add(parentLearning);
                        }
                        if (parentLearning.getType() ==2 && favoriteUser.getUserId() == loginUser.getUserId()){
                            favoriteLearningList.add(parentLearning);
                        }
                        if (parentLearning.getType() ==3 && favoriteUser.getUserId() == loginUser.getUserId()){
                            favoriteNutritionList.add(parentLearning);
                        }
                    }
                }

                mItemFavoriteDoAndDonotAdapter.setFavoriteDoAndDonotData(favoriteDoAndDonotList);
                mItemFavoriteListLearningAdapter.setFavoriteLearningData(favoriteLearningList);
                mItemFavoriteListNutritionAdapter.setFavoriteNutritionData(favoriteNutritionList);
            }
        } else {
            Snackbar.make(rvFavorite,getResources().getString(R.string.need_to_login), Snackbar.LENGTH_INDEFINITE).show();
        }
    }

}
