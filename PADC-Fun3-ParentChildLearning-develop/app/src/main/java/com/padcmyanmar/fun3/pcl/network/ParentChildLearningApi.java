package com.padcmyanmar.fun3.pcl.network;

import com.padcmyanmar.fun3.pcl.data.vo.BabyInfoVO;
import com.padcmyanmar.fun3.pcl.network.responses.BabyInfoResponse;
import com.padcmyanmar.fun3.pcl.data.vo.CurrentWeightAndHeightVO;
import com.padcmyanmar.fun3.pcl.network.responses.AddBabyDevelopmentResponse;
import com.padcmyanmar.fun3.pcl.network.responses.GetBabyDevelopmentsResponse;
import com.padcmyanmar.fun3.pcl.network.responses.GetChildLearningResponse;
import com.padcmyanmar.fun3.pcl.network.responses.GetParentLearningResponse;
import com.padcmyanmar.fun3.pcl.network.responses.GetStudentProfilesResponse;
import com.padcmyanmar.fun3.pcl.network.responses.LoginResponse;
import com.padcmyanmar.fun3.pcl.network.responses.RegisterResponse;


import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by daewichan on 3/4/18.
 */

public interface ParentChildLearningApi {
    @FormUrlEncoded
    @POST("getChildrenLessons.php")
    Call<GetChildLearningResponse> loadChildLearning(@Field("page") int page,
                                                     @Field("access_token") String accessToken);

    @FormUrlEncoded
    @POST("getParentLessons.php")
    Call<GetParentLearningResponse>loadParentLearning(@Field("page") int page,
                                                @Field("access_token") String accessToken);

    @FormUrlEncoded
    @POST("login.php")
    Call<LoginResponse> loginUser(@Field("phone") String phoneNo,
                                  @Field("password") String password);

    @FormUrlEncoded
    @POST("register.php")
    Call<RegisterResponse> registerUser(@Field("name") String name,
                                        @Field("phone") String phoneNo,
                                        @Field("date_of_birth") String dateOfBirth,
                                        @Field("country_of_origin") String country,
                                        @Field("password") String password);
  

    @POST("addBabyInfo.php?access_token=b002c7e1a528b7cb460933fc2875e916")
    Call<BabyInfoResponse> SaveBabyInfo(@Body BabyInfoVO babyInfoVO);

    @POST("updateBabyInfo.php?access_token=b002c7e1a528b7cb460933fc2875e916")
    Call<AddBabyDevelopmentResponse> babyDevelopment(@Body CurrentWeightAndHeightVO babyDevelopment);

    @FormUrlEncoded
    @POST("getBabyDevelopments.php")
    Call<GetBabyDevelopmentsResponse> loadBabyDevelopments(@Field("page") int page,
                                                        @Field("access_token") String accessToken);

    @FormUrlEncoded
    @POST("getStudentProfiles.php")
    Call<GetStudentProfilesResponse> loadStudentProfiles(@Field("access_token") String accessToken);
}
