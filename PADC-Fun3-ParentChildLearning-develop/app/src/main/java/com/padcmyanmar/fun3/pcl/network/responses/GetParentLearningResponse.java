package com.padcmyanmar.fun3.pcl.network.responses;

import com.google.gson.annotations.SerializedName;
import com.padcmyanmar.fun3.pcl.data.vo.ParentLearningVO;

import java.util.List;

/**
 * Created by daewichan on 3/3/18.
 */

public class GetParentLearningResponse {
    @SerializedName("code")
    private int code;

    @SerializedName("message")
    private String message;

    @SerializedName("apiVersion")
    private String apiVersion;

    @SerializedName("page")
    private String page;

    @SerializedName("forParents")
    private List<ParentLearningVO> forParent = null;

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String getApiVersion() {
        return apiVersion;
    }

    public String getPage() {
        return page;
    }

    public List<ParentLearningVO> getForParent() {
        return forParent;
    }

}
