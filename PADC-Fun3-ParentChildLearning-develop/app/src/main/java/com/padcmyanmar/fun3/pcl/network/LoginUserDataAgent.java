package com.padcmyanmar.fun3.pcl.network;

import android.content.Context;

/**
 * Created by User on 3/8/2018.
 */

public interface LoginUserDataAgent {
    /**
     * Login user.
     * @param phoneNo
     * @param password
     * @param context
     */
    void LoginUser(String phoneNo, String password, Context context);
}
