package com.padcmyanmar.fun3.pcl.data.vo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by htoo on 3/30/2018.
 */

public class FavoriteUserVO {
    @SerializedName("user_id")
    private int userId;

    @SerializedName("user_name")
    private String userName;

    public int getUserId() {
        return userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
