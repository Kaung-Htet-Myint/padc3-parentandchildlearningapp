package com.padcmyanmar.fun3.pcl.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.padcmyanmar.fun3.pcl.R;
import com.padcmyanmar.fun3.pcl.data.vo.ParentLearningVO;
import com.padcmyanmar.fun3.pcl.delegates.LearningTipsDelegate;
import com.padcmyanmar.fun3.pcl.viewholders.ItemLearningTipsHolder;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by daewichan on 2/19/18.
 */

public class LearningTipsAdapter extends RecyclerView.Adapter<ItemLearningTipsHolder> {

    private List<ParentLearningVO> mParentLearningList = new ArrayList<>();
    private LearningTipsDelegate mLearningTipsDelegate;

    public LearningTipsAdapter(LearningTipsDelegate learningTipsDelegate) {
        this.mLearningTipsDelegate = learningTipsDelegate;
    }

    @Override
    public ItemLearningTipsHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Context context = parent.getContext();

        LayoutInflater inflater = LayoutInflater.from(context);

        View guidesItemView = inflater.inflate(R.layout.item_learning_tips, parent, false);

        ItemLearningTipsHolder itemLearningTipsHolder = new ItemLearningTipsHolder(guidesItemView, mLearningTipsDelegate);

        return itemLearningTipsHolder;
    }

    @Override
    public void onBindViewHolder(ItemLearningTipsHolder holder, int position) {
        if (mParentLearningList.size() > 0) {
            holder.showLearningTip(mParentLearningList.get(position));
        }

    }

    public void addAll(List<ParentLearningVO> mParentLearningList) {
        this.mParentLearningList = mParentLearningList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {

        return mParentLearningList.size();
    }

}
