package com.padcmyanmar.fun3.pcl;

import android.app.Application;

import com.padcmyanmar.fun3.pcl.data.model.ParentLearningModel;

/**
 * Created by aung on 2/17/18.
 */

public class PCLApp extends Application {
    public static final String LOG_TAG = "ParentChildLearningApp";

    @Override
    public void onCreate() {
        super.onCreate();
        ParentLearningModel.getsObjectInstance();
    }
}
