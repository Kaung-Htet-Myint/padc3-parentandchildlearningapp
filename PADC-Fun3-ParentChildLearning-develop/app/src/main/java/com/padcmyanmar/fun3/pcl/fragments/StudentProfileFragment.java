package com.padcmyanmar.fun3.pcl.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.padcmyanmar.fun3.pcl.R;
import com.padcmyanmar.fun3.pcl.data.model.ParentLearningModel;
import com.padcmyanmar.fun3.pcl.data.vo.StudentProfileVO;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StudentProfileFragment extends BaseFragment {

    private static final String BARG_STUDENT_PHONE_NO = "BARG_STUDENT_PHONE_NO";

    @BindView(R.id.iv_student_profile_bg)
    ImageView ivStudentProfile;

    @BindView(R.id.tv_student_name)
    TextView tvStudentName;

    @BindView(R.id.tv_contributions)
    TextView tvContributions;

    @BindView(R.id.tv_goals_in_2019)
    TextView tvGoalsIn2019;

    public static StudentProfileFragment newInstance(String studentPhoneNo) {
        StudentProfileFragment fragment = new StudentProfileFragment();
        Bundle bundle = new Bundle();
        bundle.putString(BARG_STUDENT_PHONE_NO, studentPhoneNo);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_student_profile, container, false);
        ButterKnife.bind(this, view);

        Bundle bundle = getArguments();
        if (bundle != null) {
            String studentPhoneNo = bundle.getString(BARG_STUDENT_PHONE_NO);
            bindData(studentPhoneNo);
        }

        return view;
    }

    private void bindData(String studentPhoneNo) {
        StudentProfileVO studentProfile = ParentLearningModel.getsObjectInstance().getStudentByPhoneNo(studentPhoneNo);
        if (studentProfile != null) {
            tvStudentName.setText(studentProfile.getName());
            tvContributions.setText(studentProfile.getContributionsToProject());
            tvGoalsIn2019.setText(studentProfile.getGoalsIn2019());

            Glide.with(getContext())
                    .load(studentProfile.getPhoto())
                    .into(ivStudentProfile);
        }
    }
}
