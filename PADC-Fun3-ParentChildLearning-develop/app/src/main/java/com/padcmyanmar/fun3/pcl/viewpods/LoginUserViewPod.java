package com.padcmyanmar.fun3.pcl.viewpods;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.padcmyanmar.fun3.pcl.R;
import com.padcmyanmar.fun3.pcl.data.vo.LoginVO;
import com.padcmyanmar.fun3.pcl.delegates.BeforeLoginDelegate;
import com.padcmyanmar.fun3.pcl.delegates.LoginUserDelegate;

import java.util.concurrent.TimeoutException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by htoo on 2/18/2018.
 */

public class LoginUserViewPod extends RelativeLayout {

    @BindView(R.id.tv_name)
    TextView tvName;

    @BindView(R.id.tv_login_user_phone)
    TextView tvLoginUserPhone;

    @BindView(R.id.iv_user_profile)
    ImageView ivUserProfile;

    @BindView(R.id.iv_user_background)
    ImageView ivUserBackground;

    private LoginVO loginVO;
    public LoginUserDelegate mDelegate;

    public LoginUserViewPod(Context context) {
        super(context);
    }

    public LoginUserViewPod(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LoginUserViewPod(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this,this);
    }

    public void bindData(LoginVO login)
    {
        loginVO = login;
        tvName.setText(login.getName());
        tvLoginUserPhone.setText(login.getPhoneNo());

        Glide.with(ivUserProfile)
                .load(loginVO.getProfileUrl())
                .into(ivUserProfile);

        Glide.with(ivUserBackground)
                .load(loginVO.getCoverUrl())
                .into(ivUserBackground);

        String address=login.getAddress().toString();
        String name=login.getName().toString();
        String phoneNo=login.getPhoneNo().toString();
        int userId=login.getUserId();

    }

    public void setDelegate(LoginUserDelegate loginUserdelegate){ mDelegate = loginUserdelegate;}

    @OnClick(R.id.btn_logout)
    public void onTapLogout(View view){mDelegate.onTapLogout();}
}
