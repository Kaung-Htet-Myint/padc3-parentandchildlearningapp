package com.padcmyanmar.fun3.pcl.delegates;

import com.padcmyanmar.fun3.pcl.data.vo.ParentLearningVO;

/**
 * Created by htoo on 2/23/2018.
 */

public interface FavoriteDelegate {
    void onTapFavoriteDoAndDonotDetails(ParentLearningVO parentLearningDoAndDonot);
    void onTapFavoriteLearningDetails(ParentLearningVO parentLearning);
    void onTapFavoriteNutritionDetails(ParentLearningVO parentLearningNutrition);
    void onTapShareButton();
}
