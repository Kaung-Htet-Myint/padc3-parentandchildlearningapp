package com.padcmyanmar.fun3.pcl.network.responses;

import com.google.gson.annotations.SerializedName;
import com.padcmyanmar.fun3.pcl.data.vo.BabyDevelopmentVO;

import java.util.List;

/**
 * Created by htoo on 3/21/2018.
 */

public class GetBabyDevelopmentsResponse {
    private int code;
    private String message;
    private String apiVersion;
    private String page;
    @SerializedName("babyDevelopments")
    private List<BabyDevelopmentVO> babyDevelopments;

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String getApiVersion() {
        return apiVersion;
    }

    public String getPage() {
        return page;
    }

    public List<BabyDevelopmentVO> getBabyDevelopments() {
        return babyDevelopments;
    }
}
