package com.padcmyanmar.fun3.pcl.delegates;

import com.padcmyanmar.fun3.pcl.data.vo.ParentLearningVO;

/**
 * Created by einandartun on 2/20/18.
 */

public interface ParentDelegate {
    void onTapDoAndDonotItem(ParentLearningVO parentLearning);
    void onTapMostPopularItem(ParentLearningVO parentLearning);
}
