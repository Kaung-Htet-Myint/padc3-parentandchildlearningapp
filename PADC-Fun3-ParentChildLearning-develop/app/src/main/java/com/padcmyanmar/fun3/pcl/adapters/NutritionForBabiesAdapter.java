package com.padcmyanmar.fun3.pcl.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.padcmyanmar.fun3.pcl.R;
import com.padcmyanmar.fun3.pcl.data.vo.ParentLearningVO;
import com.padcmyanmar.fun3.pcl.delegates.ParentLearningActionDelegates;
import com.padcmyanmar.fun3.pcl.viewholders.NutritionForBabiesViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 2/20/2018.
 */

public class NutritionForBabiesAdapter extends RecyclerView.Adapter<NutritionForBabiesViewHolder>{

    public static NutritionForBabiesAdapter nutritionForBabiesAdapter;
    private ParentLearningActionDelegates mParentLearningActionDelegates;
    private List<ParentLearningVO> mParentLearningList;

    public NutritionForBabiesAdapter(ParentLearningActionDelegates parentLearningActionDelegates) {
            mParentLearningActionDelegates = parentLearningActionDelegates;
            mParentLearningList = new ArrayList<>();
    }

    @Override
    public NutritionForBabiesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View nutritionItemView = inflater.inflate(R.layout.item_nutrition_for_babies,parent,false);

        NutritionForBabiesViewHolder itemNutritionViewHolder = new NutritionForBabiesViewHolder(nutritionItemView, mParentLearningActionDelegates);
        return itemNutritionViewHolder;
    }

    @Override
    public void onBindViewHolder(NutritionForBabiesViewHolder holder, int position) {
        holder.setParentLearningData(mParentLearningList.get(position));
    }

    @Override
    public int getItemCount() {
        return mParentLearningList.size();
    }

    @Override
    public long getItemId(int position) {
        return mParentLearningList.get(position).getId();
       // return super.getItemId(position);
}

    public void setData(List<ParentLearningVO> parentLearningDataList)
    {
                mParentLearningList = parentLearningDataList;
                notifyDataSetChanged();

    }

}
