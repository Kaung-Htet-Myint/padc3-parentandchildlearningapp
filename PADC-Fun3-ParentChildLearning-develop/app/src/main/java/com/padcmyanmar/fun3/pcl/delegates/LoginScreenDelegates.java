package com.padcmyanmar.fun3.pcl.delegates;

/**
 * Created by User on 2/26/2018.
 */

public interface LoginScreenDelegates {
    void onTapToLogin();
    void onTapTORegister();
    void onTapLoginWithGoogle();
}
