package com.padcmyanmar.fun3.pcl.delegates;

/**
 * Created by User on 3/7/2018.
 */

public interface LoginUserDelegate {
    void onTapLogout();
    void onTapBabyInfo();
    void onTapBayDevelopment();
}
