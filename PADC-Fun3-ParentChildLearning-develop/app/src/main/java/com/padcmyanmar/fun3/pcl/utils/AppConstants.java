package com.padcmyanmar.fun3.pcl.utils;

/**
 * Created by aung on 12/3/17.
 */

public class AppConstants {

    public static final String BASE_URL = "http://padcmyanmar.com/padc-3/final-projects/parent-child-learning/fun/";
    public static final String LOGIN_USER_DATA_SP="PCL-Login-User-Data";
    public static final String LOGIN_USER_ID = "login-user-id";
    public static final String LOGIN_USER_NAME = "login-user-name";
    public static final String LOGIN_USER_ADDRESS = "login-user_address";
    public static final String LOGIN_USER_EMAIL = "login-user-email";
    public static final String LOGIN_USER_PHONE_NO = "login-user-phone-no";
    public static final String LOGIN_USER_PROFILE_URL = "login-user-profile-url";
    public static final String LOGIN_COVER_URL= "login-user-cover-url";

    public static final String TEMP_ACCESS_TOKEN = "b002c7e1a528b7cb460933fc2875e916";
}
