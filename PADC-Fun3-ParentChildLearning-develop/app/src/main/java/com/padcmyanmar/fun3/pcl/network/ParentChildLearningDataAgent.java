package com.padcmyanmar.fun3.pcl.network;

import com.padcmyanmar.fun3.pcl.data.vo.BabyInfoVO;
import android.content.Context;
import com.padcmyanmar.fun3.pcl.data.vo.CurrentWeightAndHeightVO;

/**
 * Created by daewichan on 3/4/18.
 */

public interface ParentChildLearningDataAgent {

    void loadLearning();

    void loadParentLearning();

    void registerUser(String name, String phoneNumber, String dateOfBirth, String country, String password);

    void SaveBabyInfo (BabyInfoVO babyInfoVO);
  
    void addBabyDevelopment(CurrentWeightAndHeightVO babyDevelopmentVO);

    void loadBabyDevelopments();

    void loadStudentProfiles(String accessToken);

}
