package com.padcmyanmar.fun3.pcl.delegates;

import com.padcmyanmar.fun3.pcl.data.vo.ParentLearningVO;

/**
 * Created by daewichan on 2/21/18.
 */

public interface LearningTipsDelegate {
    //void onTapItem();
    void onTapItem(ParentLearningVO parentLearningVO);

}
